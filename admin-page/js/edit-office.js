$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";
    var gPhone = "";

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện click cho nút Save Office
    Array.from($("#form-edit")).forEach(form => {
        form.addEventListener("submit", event => {
            event.preventDefault();
            event.stopPropagation();
            if (form.checkValidity()) {
                onBtnUpdateClick();
            }
            form.classList.add("was-validated");
        }, false);
    });

    //Gán sự kiện cho nút Cancel
    $("#btn-cancel").on("click", function () {
        window.history.back();
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        const vUrlParams = new URLSearchParams(window.location.search);
        gId = vUrlParams.get("id");
        var token = getCookie("token");
        if (token) {
            if (gId != "" && gId != null) {
                $.ajax({
                    url: gBASE_URL + "offices/" + gId,
                    method: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    success: function (res) {
                        loadEditData(res);
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi ấn nút Update
    function onBtnUpdateClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vOffice = {
                city: "",
                phone: "",
                addressLine: "",
                territory: "",
                state: "",
                country: ""
            }
            //Lấy dữ liệu
            getOfficeData(vOffice);
            //Kiểm tra dữ liệu
            var vIsCheck = gPhone == vOffice.phone ? true : !checkPhone(vOffice.phone);
            if (vIsCheck) {
                $.ajax({
                    url: gBASE_URL + "offices/" + gId,
                    method: "PUT",
                    async: false,
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vOffice),
                    success: function (res) {
                        window.location.href = "offices.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            } else {
                toastr.warning("Phone already exists!");
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm xử lý hiển thị dữ liệu Office
    function loadEditData(paramData) {
        "use strict";
        $("#inp-city").val(paramData.city);
        gPhone = paramData.phone;
        $("#inp-phone").val(gPhone);
        $("#inp-addressLine").val(paramData.addressLine);
        $("#inp-territory").val(paramData.territory);
        $("#inp-state").val(paramData.state);
        $("#inp-country").val(paramData.country);
    }

    //Hàm lấy dữ liệu Office
    function getOfficeData(paramData) {
        paramData.city = $("#inp-city").val().trim();
        paramData.phone = $("#inp-phone").val().trim();
        paramData.addressLine = $("#inp-addressLine").val().trim();
        paramData.territory = $("#inp-territory").val().trim();
        paramData.state = $("#inp-state").val().trim();
        paramData.country = $("#inp-country").val().trim();
    }

    //Hàm kiểm tra phone
    function checkPhone(paramPhone) {
        "use strict";
        var vIsCheck = true;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "offices/phone/" + paramPhone + "/exists",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    vIsCheck = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
        return vIsCheck;
    }
});