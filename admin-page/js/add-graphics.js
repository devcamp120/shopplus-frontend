$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    $("#sel-graphicsBrand").on("change", onSelectGraphicsBrandChange);

    //Gán sự kiện click cho nút Add
    Array.from($("#form-add")).forEach(form => {
        form.addEventListener("submit", event => {
            event.preventDefault();
            event.stopPropagation();
            if (form.checkValidity()) {
                onBtnAddClick();
            }
            form.classList.add("was-validated");
        }, false);
    });

    //Gán sự kiện cho nút Cancel
    $("#btn-cancel").on("click", function () {
        window.history.back();
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "graphics-brands/all",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    loadDataToSelectGraphicsBrand(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi ấn nút Add
    function onBtnAddClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vGraphics = {
                graphicsBrand: "",
                graphicsType: "",
                gpuName: "",
                gpuMemory: ""
            }
            //Lấy dữ liệu
            getGraphicsData(vGraphics);
            //Kiểm tra dữ liệu
            var vIsCheck = !checkGpuName(vGraphics.gpuName);

            if (vIsCheck) {
                $.ajax({
                    url: gBASE_URL + "graphics",
                    method: "POST",
                    async: false,
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vGraphics),
                    success: function (res) {
                        window.location.href = "graphics.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            } else {
                toastr.warning("GPU Name already exists!");
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi chọn Graphics Brand
    function onSelectGraphicsBrandChange() {
        var vGraphicsBrandId = $("#sel-graphicsBrand").val();
        $("#sel-graphicsType").html("");
        $("#sel-graphicsType").append($("<option>").val("").text("Graphics Type"));
        if (vGraphicsBrandId != "") {
            var token = getCookie("token");
            if (token) {
                $.ajax({
                    url: gBASE_URL + "graphics-brands/" + vGraphicsBrandId + "/graphics-types",
                    method: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    async: false,
                    success: function (res) {
                        $("#sel-graphicsType").prop("disabled", false);
                        loadDataToSelectGraphicsType(res);
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            } else {
                toastr.error("You are not logged in!");
            }
        } else {
            $("#sel-graphicsType").prop("disabled", true);
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm load dữ liệu vào Select Graphics Brand
    function loadDataToSelectGraphicsBrand(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-graphicsBrand").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Graphics Type
    function loadDataToSelectGraphicsType(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-graphicsType").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm lấy dữ liệu Graphics
    function getGraphicsData(paramData) {
        paramData.graphicsBrand = {
            id: $("#sel-graphicsBrand").val().trim()
        }
        var vGraphicsTypeId = $("#sel-graphicsType").val().trim();
        if (vGraphicsTypeId != "") {
            paramData.graphicsType = {
                id: vGraphicsTypeId
            }
        } else {
            paramData.graphicsType = null;
        }
        paramData.gpuName = $("#inp-gpuName").val().trim();
        paramData.gpuMemory = $("#inp-gpuMemory").val().trim();
    }

    //Hàm kiểm tra gpuName
    function checkGpuName(paramGpuName) {
        "use strict";
        var vIsCheck = true;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "graphics/gpu-name/" + paramGpuName + "/exists",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    vIsCheck = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
        return vIsCheck;
    }
});