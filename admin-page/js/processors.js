"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gTotalPages = 0;
    var gPage = 0;
    var gPageSize = 10;
    var gId = "";
    var gKeyword = "";
    const gNameCol = ["action", "id", "processorBrand", "processorCollection", "processorNumber", "totalCores", "totalThreads", "baseFrequency", "maxTurboFrequency", "cache", "processorGraphics", "graphicsFrequency"];
    const gACTION_COL = 0;
    const gID_COL = 1;
    const gPROCESSOR_BRAND_COL = 2;
    const gPROCESSOR_COLLECTION_COL = 3;
    const gPROCESSOR_NUMBER_COL = 4;
    const gTOTAL_CORES_COL = 5;
    const gTOTAL_THREADS_COL = 6;
    const gBASE_FREQUENCY_COL = 7;
    const gMAX_TURBO_FREQUENCY_COL = 8;
    const gCACHE_COL = 9;
    const gPROCESSOR_GRAPHICS_COL = 10;
    const gGRAPHICS_FREQUENCY_COL = 11;
    var gDataTable = $("#table-processors").DataTable({
        columns: [
            { data: gNameCol[gACTION_COL] },
            { data: gNameCol[gID_COL] },
            { data: gNameCol[gPROCESSOR_BRAND_COL] },
            { data: gNameCol[gPROCESSOR_COLLECTION_COL] },
            { data: gNameCol[gPROCESSOR_NUMBER_COL] },
            { data: gNameCol[gTOTAL_CORES_COL] },
            { data: gNameCol[gTOTAL_THREADS_COL] },
            { data: gNameCol[gBASE_FREQUENCY_COL] },
            { data: gNameCol[gMAX_TURBO_FREQUENCY_COL] },
            { data: gNameCol[gCACHE_COL] },
            { data: gNameCol[gPROCESSOR_GRAPHICS_COL] },
            { data: gNameCol[gGRAPHICS_FREQUENCY_COL] }
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: "<a href='javascript:void(0)' class='dropdown'><a href='javascript:void(0)' class='dropdown-toggle text-dark me-3' data-bs-toggle='dropdown' aria-expanded='false'><i class='fa-solid fa-ellipsis'></i></a><ul class='dropdown-menu'><li><a class='dropdown-item item-edit' href='javascript:void(0)'>Edit</a></li><li><a class='dropdown-item item-delete' href='javascript:void(0)'>Delete</a></li></ul></a><a href='javascript:void(0)' class='item-detail' title='Detail'><i class='text-info fa-solid fa-circle-info'></i></a>",
                className: "text-nowrap"
            },
            {
                targets: gPROCESSOR_BRAND_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                }
            },
            {
                targets: gPROCESSOR_COLLECTION_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.name;
                    }
                    return "";
                }
            },
            {
                targets: gBASE_FREQUENCY_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.toFixed(2);
                    }
                    return "";
                }
            },
            {
                targets: gMAX_TURBO_FREQUENCY_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.toFixed(2);
                    }
                    return "";
                }
            },
            {
                targets: gGRAPHICS_FREQUENCY_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.toFixed(2);
                    }
                    return "";
                }
            }
        ],
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        scrollY: 400,
        scrollX: true,
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện khi nhập vào ô tìm kiếm
    $("#inp-search").on("input", function () {
        onSearchInput();
    });

    //Gán sự kiện cho nút add
    $("#add-processor").on("click", function () {
        onAddProcessorClick();
    });

    //Gán sự kiện cho nút Detail
    $("#table-processors tbody").on("click", ".item-detail", function () {
        onItemDetailClick(this);
    });

    //Gán sự kiện cho nút Edit
    $("#table-processors tbody").on("click", ".item-edit", function () {
        onItemEditClick(this);
    });

    //Gán sự kiện cho nút Delete
    $("#table-processors tbody").on("click", ".item-delete", function () {
        onItemDeleteClick(this);
    });

    //Gán sự kiện cho nút Edit trên modal Detail
    $("#btn-edit").on("click", onModalDetailEditClick);

    //Gán sự kiện cho nút Delete trên modal Detail
    $("#btn-delete").on("click", onModalDetailDeleteClick);

    //Gán sự kiện cho nút Delete trên modal
    $("#btn-delete-confirm").on("click", onBtnDeleteConfirmClick);

    //Gán sự kiện cho các nút phân trang
    $("#number-page").on("keypress", function (event) {
        if (event.which == 13) {
            onNumberPageEnter();
        }
    });
    $("#sel-page-size").on("change", onChangePageSize);
    $("#first-page").on("click", onFirstPageClick);
    $("#previous-page").on("click", onPreviousPageClick);
    $("#next-page").on("click", onNextPageClick);
    $("#last-page").on("click", onLastPageClick);

    //Điều chỉnh lại các cột table khi thay đổi kích thước window
    $(window).resize(function () {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        loadProcessorDataToTable();
    }

    // Hàm xử lý khi nhập vào ô tìm kiếm
    function onSearchInput() {
        "use strict";
        gKeyword = $("#inp-search").val();
        gPage = 0;
        loadProcessorDataToTable();

    }

    // Hàm xử lý khi click nút add
    function onAddProcessorClick() {
        "use strict";
        window.location.href = "add-processor.html";
    }

    // Hàm xử lý khi click nút Detail
    function onItemDetailClick(paramBtnDetail) {
        "use strict";
        gId = gDataTable.row(paramBtnDetail.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "processors/" + gId,
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    loadDetailData(res);
                    $("#modal-detail").modal("show");
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    // Hàm xử lý khi click nút edit
    function onItemEditClick(paramBtnDetail) {
        "use strict";
        gId = gDataTable.row(paramBtnDetail.closest("tr")).data().id;
        window.location.href = "edit-processor.html?id=" + gId;
    }

    // Hàm xử lý khi click nút Delete
    function onItemDeleteClick(paramBtnDetail) {
        "use strict";
        gId = gDataTable.row(paramBtnDetail.closest("tr")).data().id;
        $("#modal-delete").modal("show");
    }

    // Hàm xử lý khi click nút Edit trên Modal Detail
    function onModalDetailEditClick() {
        "use strict";
        $("#modal-detail").modal("hide");
        window.location.href = "edit-processor.html?id=" + gId;
    }

    // Hàm xử lý khi click nút Delete trên Modal Detail
    function onModalDetailDeleteClick() {
        "use strict";
        $("#modal-detail").modal("hide");
        $("#modal-delete").modal("show");
    }

    // Hàm xử lý khi click nút delete trên modal
    function onBtnDeleteConfirmClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "processors/" + gId,
                method: "DELETE",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    $("#modal-delete").modal("hide");
                    loadProcessorDataToTable();
                }, error: function (err) {
                    $("#modal-delete").modal("hide");
                    $("#modal-error").modal("show");
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    function onNumberPageEnter() {
        var vPage = $("#number-page").val();
        if (vPage < 1) {
            gPage = 0;
            loadProcessorDataToTable();
        } else if (vPage > gTotalPages && gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadProcessorDataToTable();
        } else {
            gPage = vPage - 1;
            loadProcessorDataToTable();
        }
    }

    function onChangePageSize() {
        gPage = 0;
        gPageSize = $("#sel-page-size").val();
        loadProcessorDataToTable();
    }

    //Hàm xử lý khi ấn nút Trang đầu
    function onFirstPageClick() {
        if (gTotalPages > 0) {
            gPage = 0;
            loadProcessorDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang trước
    function onPreviousPageClick() {
        if (gTotalPages > 0 && gPage > 0) {
            gPage--;
            loadProcessorDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang sau
    function onNextPageClick() {
        if (gTotalPages > 0 && gPage < gTotalPages - 1) {
            gPage++;
            loadProcessorDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang cuối
    function onLastPageClick() {
        if (gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadProcessorDataToTable();
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm load dữ liệu Processor vào bảng
    function loadProcessorDataToTable() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "processors?keyword=" + gKeyword + "&page=" + gPage + "&size=" + gPageSize,
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    gDataTable.clear();
                    gDataTable.rows.add(res.content);
                    gDataTable.draw();
                    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
                    gTotalPages = res.totalPages;
                    $("#number-page").val(gPage + 1);
                    $("#element-first").html(gPage * gPageSize + (res.numberOfElements !== 0 ? 1 : 0));
                    $("#element-last").html(gPage * gPageSize + res.numberOfElements);
                    $("#total-elements").html(res.totalElements);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý hiển thị chi tiết
    function loadDetailData(paramData) {
        "use strict";
        $("#p-id").html(paramData.id);
        if (paramData.processorBrand != null) {
            $("#p-processorBrand").html(paramData.processorBrand.name);
        } else {
            $("#p-processorBrand").html("");
        }
        if (paramData.processorCollection != null) {
            $("#p-processorCollection").html(paramData.processorCollection.name);
        } else {
            $("#p-processorCollection").html("");
        }
        $("#p-processorNumber").html(paramData.processorNumber);
        $("#p-totalCores").html(paramData.totalCores);
        $("#p-totalThreads").html(paramData.totalThreads);
        if (paramData.baseFrequency != null) {
            $("#p-baseFrequency").html(paramData.baseFrequency.toFixed(2));
        } else {
            $("#p-baseFrequency").html("");
        }
        if (paramData.maxTurboFrequency != null) {
            $("#p-maxTurboFrequency").html(paramData.maxTurboFrequency.toFixed(2));
        } else {
            $("#p-maxTurboFrequency").html("");
        }
        $("#p-cache").html(paramData.cache);
        $("#p-processorGraphics").html(paramData.processorGraphics);
        if (paramData.graphicsFrequency != null) {
            $("#p-graphicsFrequency").html(paramData.graphicsFrequency.toFixed(2));
        } else {
            $("#p-graphicsFrequency").html("");
        }
    }
});