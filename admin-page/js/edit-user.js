$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";
    var gIsAdmin = false;

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện click cho nút Save User
    Array.from($("#form-edit")).forEach(form => {
        form.addEventListener("submit", event => {
            event.preventDefault();
            event.stopPropagation();
            if (form.checkValidity()) {
                onBtnUpdateClick();
            }
            form.classList.add("was-validated");
        }, false);
    });

    //Gán sự kiện cho nút Cancel
    $("#btn-cancel").on("click", function () {
        window.history.back();
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        const vUrlParams = new URLSearchParams(window.location.search);
        gId = vUrlParams.get("id");

        $("#sel-roles").select2({
            theme: "bootstrap-5",
        });

        var token = getCookie("token");
        if (token) {
            if (gId != "" && gId != null) {
                $.ajax({
                    url: gBASE_URL + "users/" + gId,
                    method: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    success: function (res) {
                        loadEditData(res);
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
            $.ajax({
                url: gBASE_URL + "auth/user-info",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    gIsAdmin = res.strRoles.includes("ROLE_ADMIN");
                    if(gIsAdmin) {
                        $("#hidden-password").prop("hidden", false);
                        $("#hidden-roles").prop("hidden", false);
                        $("#hidden-activated").prop("hidden", false);
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi ấn nút Update
    function onBtnUpdateClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vUser = {
                firstName: "",
                lastName: "",
                password: "",
                address: "",
                city: "",
                state: "",
                postalCode: "",
                country: "",
                strRoles: "",
                activated: ""
            }
            //Lấy dữ liệu
            getUserData(vUser);
            var vUrl = "";
            if(gIsAdmin) {
                vUrl = gBASE_URL + "users/" + gId + "/update-user-by-admin";
            } else {
                vUrl = gBASE_URL + "users/" + gId + "/update-user-by-seller";
            }
            $.ajax({
                url: vUrl,
                method: "PUT",
                async: false,
                headers: {
                    Authorization: "Bearer " + token,
                    "Content-Type": "application/json"
                },
                data: JSON.stringify(vUser),
                success: function (res) {
                    window.location.href = "users.html";
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm xử lý hiển thị dữ liệu User
    function loadEditData(paramData) {
        "use strict";
        $("#inp-firstName").val(paramData.firstName);
        $("#inp-lastName").val(paramData.lastName);
        $("#inp-username").val(paramData.username);
        $("#inp-address").val(paramData.address);
        $("#inp-city").val(paramData.city);
        $("#inp-state").val(paramData.state);
        $("#inp-postalCode").val(paramData.postalCode);
        $("#inp-country").val(paramData.country);
        if (paramData.activated != null) {
            $("#cbx-activated").prop("checked", paramData.activated);
        }
        if (paramData.roles != null) {
            var vRoles = [];
            for (var bI = 0; bI < paramData.roles.length; bI++) {
                vRoles.push(paramData.roles[bI].roleKey);
            }
            $("#sel-roles").val(vRoles).trigger("change");
        }
    }

    //Hàm lấy dữ liệu User
    function getUserData(paramData) {
        "use strict";
        paramData.firstName = $("#inp-firstName").val().trim();
        paramData.lastName = $("#inp-lastName").val().trim();
        paramData.password = $("#inp-password").val().trim();
        paramData.address = $("#inp-address").val().trim();
        paramData.city = $("#inp-city").val().trim();
        paramData.state = $("#inp-state").val().trim();
        paramData.postalCode = $("#inp-postalCode").val().trim();
        paramData.country = $("#inp-country").val().trim();
        paramData.strRoles = $("#sel-roles").val();
        paramData.activated = $("#cbx-activated").is(":checked");
    }
});