$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";
    var gName = "";
    var gProductBrandPhotoId = "";
    var gDeletePhotoId = "";

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện click cho nút Save ProductBrand
    Array.from($("#form-edit")).forEach(form => {
        form.addEventListener("submit", event => {
            event.preventDefault();
            event.stopPropagation();
            if (form.checkValidity()) {
                onBtnUpdateClick();
            }
            form.classList.add("was-validated");
        }, false);
    });

    //Gán sự kiện cho nút Cancel
    $("#btn-cancel").on("click", function () {
        window.history.back();
    });

    $("#card-images").on("click", ".delete-server-image", function () {
        gDeletePhotoId = gProductBrandPhotoId;
        $(this).parent().parent().remove();
    });

    $("#card-images").on("click", ".delete-local-image", function () {
        $("#file-photo").val("");
        $("#card-images .delete-local-image").parent().parent().remove();
    });

    $("#file-photo").on("change", function () {
        $("#card-images").html("");
        gDeletePhotoId = gProductBrandPhotoId;
        var vFiles = this.files;
        for (var bI = 0; bI < vFiles.length; bI++) {
            if (vFiles[bI]) {
                let reader = new FileReader();
                reader.onload = function (event) {
                    $("#card-images").append(
                        "<div class='col'>"
                        + "<div class='card border-0'>"
                        + "<img src='" + event.target.result + "' class='card-img-top' style='object-fit: contain;' alt='...'>"
                        + "<button class='btn border-0 delete-local-image' style='position: absolute; top: 0%; left: 85%;'><i class='text-danger fas fa-window-close'></i></button>"
                        + "</div>"
                        + "</div>"
                    );
                }
                reader.readAsDataURL(vFiles[bI]);
            }
        }
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        const vUrlParams = new URLSearchParams(window.location.search);
        gId = vUrlParams.get("id");
        var token = getCookie("token");
        if (token) {
            if (gId != "" && gId != null) {
                $.ajax({
                    url: gBASE_URL + "product-brands/" + gId,
                    method: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    success: function (res) {
                        loadEditData(res);
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi ấn nút Update
    function onBtnUpdateClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vProductBrand = {
                name: "",
                description: "",
            }
            //Lấy dữ liệu
            getProductBrandData(vProductBrand);
            //Kiểm tra dữ liệu
            var vIsCheck = gName == vProductBrand.name ? true : !checkName(vProductBrand.name);
            if (vIsCheck) {
                $.ajax({
                    url: gBASE_URL + "product-brands/" + gId,
                    method: "PUT",
                    async: false,
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vProductBrand),
                    success: function (res) {
                        if (gDeletePhotoId != "") {
                            $.ajax({
                                url: gBASE_URL + "product-brand-photos/" + gDeletePhotoId,
                                method: "DELETE",
                                async: false,
                                headers: {
                                    Authorization: "Bearer " + token,
                                    "Content-Type": "application/json"
                                },
                                success: function (res) {

                                },
                                error: function (err) {
                                    console.log(err.responseText);
                                }
                            });
                        }
                        if ($("#file-photo")[0].files.length > 0) {
                            var vForm = getPhotoInputData();
                            $.ajax({
                                url: gBASE_URL + "product-brand-photos?productBrandId=" + res.id,
                                method: "POST",
                                headers: {
                                    Authorization: "Bearer " + token
                                },
                                async: false,
                                processData: false,
                                mimeType: "multipart/form-data",
                                contentType: false,
                                data: vForm,
                                success: function (res) {
                                    window.location.href = "product-brands.html";
                                },
                                error: function (err) {
                                    console.log(err.responseText);
                                }
                            });
                        } else {
                            window.location.href = "product-brands.html";
                        }
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            } else {
                toastr.warning("Name already exists!");
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm xử lý hiển thị dữ liệu Product Brand
    function loadEditData(paramData) {
        "use strict";
        gName = paramData.name;
        $("#inp-name").val(gName);
        if (paramData.productBrandPhoto != null) {
            gProductBrandPhotoId = paramData.productBrandPhoto.id;
            $("#card-images").append(
                "<div class='col'>"
                + "<div class='card border-0'>"
                + "<img src='" + gBASE_URL + "product-brand-photos/" + paramData.productBrandPhoto.name + "' class='card-img-top' style='object-fit: contain;' alt='...'>"
                + "<button class='btn border-0 delete-server-image' style='position: absolute; top: 0%; left: 80%;'><i class='text-danger fas fa-window-close'></i></button>"
                + "</div>"
                + "</div>"
            );
        }
        $("#inp-description").val(paramData.description);
    }

    //Hàm lấy dữ liệu Product Brand
    function getProductBrandData(paramData) {
        paramData.name = $("#inp-name").val().trim();
        paramData.description = $("#inp-description").val().trim();
    }

    //Hàm lấy dữ liệu hình ảnh để upload
    function getPhotoInputData() {
        var formData = new FormData();
        var vFiles = $("#file-photo")[0].files;
        formData.append("file", vFiles[0]);
        return formData;
    }

    //Hàm kiểm tra name
    function checkName(paramName) {
        "use strict";
        var vIsCheck = true;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "product-brands/name/" + paramName + "/exists",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    vIsCheck = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
        return vIsCheck;
    }
});