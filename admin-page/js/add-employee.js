$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện click cho nút Add
    Array.from($("#form-add")).forEach(form => {
        form.addEventListener("submit", event => {
            event.preventDefault();
            event.stopPropagation();
            if (form.checkValidity()) {
                onBtnAddClick();
            }
            form.classList.add("was-validated");
        }, false);
    });

    //Gán sự kiện cho nút Cancel
    $("#btn-cancel").on("click", function () {
        window.history.back();
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";

        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "offices/all",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    loadDataToSelectOffice(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });

            $("#sel-reportTo").select2({
                theme: "bootstrap-5",
                placeholder: "Enter First Name / Last Name / Email",
                minimumInputLength: 2,
                allowClear: true,
                ajax: {
                    url: gBASE_URL + "employees/select-search",
                    type: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    dataType: "json",
                    delay: 250,
                    data: function (params) {
                        return {
                            keyword: params.term
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (reportTo) {
                                return {
                                    text: reportTo.id + " - " + reportTo.firstName + " " + reportTo.lastName + " - " + reportTo.email,
                                    id: reportTo.id,
                                    data: reportTo
                                };
                            })
                        };
                    }
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi ấn nút Add
    function onBtnAddClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vEmployee = {
                firstName: "",
                lastName: "",
                extension: "",
                email: "",
                office: "",
                reportTo: "",
                jobTitle: ""
            }
            //Lấy dữ liệu
            getEmployeeData(vEmployee);
            //Kiểm tra dữ liệu
            var vIsCheck = !checkEmail(vEmployee.email);

            if (vIsCheck) {
                $.ajax({
                    url: gBASE_URL + "employees",
                    method: "POST",
                    async: false,
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vEmployee),
                    success: function (res) {
                        window.location.href = "employees.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            } else {
                toastr.warning("Email already exists!");
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm load dữ liệu vào Select Office
    function loadDataToSelectOffice(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bText = paramData[bI].id + " - " + paramData[bI].phone + " - " + paramData[bI].addressLine;
            $("#sel-office").append($("<option>").val(bId).text(bText));
        }
    }

    //Hàm lấy dữ liệu Employee
    function getEmployeeData(paramData) {
        paramData.firstName = $("#inp-firstName").val().trim();
        paramData.lastName = $("#inp-lastName").val().trim();
        paramData.extension = $("#inp-extension").val().trim();
        paramData.email = $("#inp-email").val().trim();
        paramData.office = {
            id: $("#sel-office").val()
        }
        paramData.reportTo = $("#sel-reportTo").val();
        paramData.jobTitle = $("#inp-jobTitle").val().trim();
    }

    //Hàm kiểm tra email
    function checkEmail(paramEmail) {
        "use strict";
        var vIsCheck = true;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "employees/email/" + paramEmail + "/exists",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    vIsCheck = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
        return vIsCheck;
    }
});