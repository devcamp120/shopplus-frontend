"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gTotalPages = 0;
    var gPage = 0;
    var gPageSize = 10;
    var gId = "";
    var gKeyword = "";
    var gSortBy = "0";
    var gProductBrandId = "-1";
    var gProcessorBrandId = "-1";
    var gEnabled = "1";
    const gNameCol = ["action", "id", "productCode", "fullName", "releaseDate", "buyPrice", "discountPercentage", "color", "processor", "graphics", "storageType", "totalStorageCapacity", "systemMemory", "screenSize", "quantityInStock"];
    const gACTION_COL = 0;
    const gID_COL = 1;
    const gPRODUCT_CODE_COL = 2;
    const gFULL_NAME_COL = 3;
    const gRELEASE_DATE_COL = 4;
    const gBUY_PRICE_COL = 5;
    const gDISCOUNT_PERCENTAGE_COL = 6;
    const gCOLOR_COL = 7;
    const gPROCESSOR_COL = 8;
    const gGRAPHICS_COL = 9;
    const gSTORAGE_TYPE_COL = 10;
    const gTOTAL_STORAGE_CAPACITY_COL = 11;
    const gSYSTEM_MEMORY_COL = 12;
    const gSCREEN_SIZE_COL = 13;
    const gQUANTITY_IN_STOCK_COL = 14;
    var gDataTable = $("#table-products").DataTable({
        columns: [
            { data: gNameCol[gACTION_COL] },
            { data: gNameCol[gID_COL] },
            { data: gNameCol[gPRODUCT_CODE_COL] },
            { data: gNameCol[gFULL_NAME_COL] },
            { data: gNameCol[gRELEASE_DATE_COL] },
            { data: gNameCol[gBUY_PRICE_COL] },
            { data: gNameCol[gDISCOUNT_PERCENTAGE_COL] },
            { data: gNameCol[gCOLOR_COL] },
            { data: gNameCol[gPROCESSOR_COL] },
            { data: gNameCol[gGRAPHICS_COL] },
            { data: gNameCol[gSTORAGE_TYPE_COL] },
            { data: gNameCol[gTOTAL_STORAGE_CAPACITY_COL] },
            { data: gNameCol[gSYSTEM_MEMORY_COL] },
            { data: gNameCol[gSCREEN_SIZE_COL] },
            { data: gNameCol[gQUANTITY_IN_STOCK_COL] }
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: "<a href='javascript:void(0)' class='dropdown'><a href='javascript:void(0)' class='dropdown-toggle text-dark me-3' data-bs-toggle='dropdown' aria-expanded='false'><i class='fa-solid fa-ellipsis'></i></a><ul class='dropdown-menu'><li><a class='dropdown-item item-edit' href='javascript:void(0)'>Edit</a></li><li><a class='dropdown-item item-delete' href='javascript:void(0)'>Delete</a></li></ul></a><a href='javascript:void(0)' class='item-detail' title='Detail'><i class='text-info fa-solid fa-circle-info'></i></a>",
                className: "text-nowrap"
            },
            {
                targets: gFULL_NAME_COL,
                width: 300,
            },
            {
                targets: gRELEASE_DATE_COL,
                className: "text-nowrap"
            },
            {
                targets: gPROCESSOR_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.fullName;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gGRAPHICS_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.fullName;
                    }
                    return "";
                },
                className: "text-nowrap"
            }
        ],
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        scrollY: 400,
        scrollX: true,
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện khi nhập vào ô tìm kiếm
    $("#inp-search").on("input", function () {
        onSearchInput();
    });

    //Gán sự kiện cho nút add
    $("#add-product").on("click", function () {
        onAddProductClick();
    });

    //Gán sự kiện cho nút Filter
    $("#filter-product").on("click", onBtnFilterClick);

    //Gán sự kiện cho nút Filter trên modal filter
    $("#btn-filter").on("click", onBtnModalFilterClick);

    //Gán sự kiện cho nút Reset trên modal filter
    $("#btn-reset-filter").on("click", onBtnModalResetFilterClick);

    //Gán sự kiện cho nút Detail
    $("#table-products tbody").on("click", ".item-detail", function () {
        onItemDetailClick(this);
    });

    //Gán sự kiện cho nút Edit
    $("#table-products tbody").on("click", ".item-edit", function () {
        onItemEditClick(this);
    });

    //Gán sự kiện cho nút Delete
    $("#table-products tbody").on("click", ".item-delete", function () {
        onItemDeleteClick(this);
    });

    //Gán sự kiện cho nút Edit trên modal Detail
    $("#btn-edit").on("click", onModalDetailEditClick);

    //Gán sự kiện cho nút Delete trên modal Detail
    $("#btn-delete").on("click", onModalDetailDeleteClick);

    //Gán sự kiện cho nút Delete trên modal
    $("#btn-delete-confirm").on("click", onBtnDeleteConfirmClick);

    //Gán sự kiện cho nút Enable / Disable trên modal Detail
    $("#btn-enable-disable").on("click", onModalDetailEnableDisableClick);

    //Gán sự kiện cho nút Enable / Disable trên modal
    $("#btn-enable-disable-confirm").on("click", onBtnEnableDisableConfirmClick);

    //Gán sự kiện cho các nút phân trang
    $("#number-page").on("keypress", function (event) {
        if (event.which == 13) {
            onNumberPageEnter();
        }
    });
    $("#sel-page-size").on("change", onChangePageSize);
    $("#first-page").on("click", onFirstPageClick);
    $("#previous-page").on("click", onPreviousPageClick);
    $("#next-page").on("click", onNextPageClick);
    $("#last-page").on("click", onLastPageClick);

    //Điều chỉnh lại các cột table khi thay đổi kích thước window
    $(window).resize(function () {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        loadProductDataToTable();

        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "product-brands/all",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    loadDataToSelectProductBrand(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });

            $.ajax({
                url: gBASE_URL + "processor-brands/all",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    loadDataToSelectProcessorBrand(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });

            $.ajax({
                url: gBASE_URL + "graphics-brands/all",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    loadDataToSelectGraphicsBrand(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    // Hàm xử lý khi nhập vào ô tìm kiếm
    function onSearchInput() {
        "use strict";
        gKeyword = $("#inp-search").val();
        gPage = 0;
        loadProductDataToTable();

    }

    // Hàm xử lý khi click nút add
    function onAddProductClick() {
        "use strict";
        window.location.href = "add-product.html";
    }

    // Hàm xử lý khi click nút Filter
    function onBtnFilterClick() {
        "use strict";
        $("#sel-sortBy").val(gSortBy);
        $("#sel-productBrand").val(gProductBrandId);
        $("#sel-processorBrand").val(gProcessorBrandId);
        $("#sel-enabled").val(gEnabled);
        $("#modal-filter").modal("show");
    }

    // Hàm xử lý khi click nút Filter trên modal filter
    function onBtnModalFilterClick() {
        "use strict";
        gSortBy = $("#sel-sortBy").val();
        gProductBrandId = $("#sel-productBrand").val();
        gProcessorBrandId = $("#sel-processorBrand").val();
        gEnabled = $("#sel-enabled").val();
        gPage = 0;
        loadProductDataToTable();
        $("#modal-filter").modal("hide");
    }

    // Hàm xử lý khi click nút Reset trên modal filter
    function onBtnModalResetFilterClick() {
        "use strict";
        gSortBy = "0";
        gProductBrandId = "-1";
        gProcessorBrandId = "-1";
        gEnabled = "1";
        loadProductDataToTable();
        $("#modal-filter").modal("hide");
    }

    // Hàm xử lý khi click nút Detail
    function onItemDetailClick(paramBtnDetail) {
        "use strict";
        gId = gDataTable.row(paramBtnDetail.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "products/" + gId,
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    loadDetailData(res);
                    $("#modal-detail").modal("show");
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    // Hàm xử lý khi click nút edit
    function onItemEditClick(paramBtnDetail) {
        "use strict";
        gId = gDataTable.row(paramBtnDetail.closest("tr")).data().id;
        window.location.href = "edit-product.html?id=" + gId;
    }

    // Hàm xử lý khi click nút Delete
    function onItemDeleteClick(paramBtnDetail) {
        "use strict";
        gId = gDataTable.row(paramBtnDetail.closest("tr")).data().id;
        $("#modal-delete").modal("show");
    }

    // Hàm xử lý khi click nút Edit trên Modal Detail
    function onModalDetailEditClick() {
        "use strict";
        $("#modal-detail").modal("hide");
        window.location.href = "edit-product.html?id=" + gId;
    }

    // Hàm xử lý khi click nút Delete trên Modal Detail
    function onModalDetailDeleteClick() {
        "use strict";
        $("#modal-detail").modal("hide");
        $("#modal-delete").modal("show");
    }

    // Hàm xử lý khi click nút Enable / Disable trên Modal Detail
    function onModalDetailEnableDisableClick() {
        "use strict";
        $("#modal-detail").modal("hide");
        $("#modal-enable-disable").modal("show");
    }

    // Hàm xử lý khi click nút delete trên modal
    function onBtnDeleteConfirmClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "products/" + gId,
                method: "DELETE",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    $("#modal-delete").modal("hide");
                    loadProductDataToTable();
                }, error: function (err) {
                    $("#modal-delete").modal("hide");
                    $("#modal-error").modal("show");
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    // Hàm xử lý khi click nút Enable / Disable trên modal
    function onBtnEnableDisableConfirmClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "products/" + gId,
                method: "PATCH",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    $("#modal-enable-disable").modal("hide");
                    loadProductDataToTable();
                }, error: function (err) {
                    console.log("ABCXYZ");
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    function onNumberPageEnter() {
        var vPage = $("#number-page").val();
        if (vPage < 1) {
            gPage = 0;
            loadProductDataToTable();
        } else if (vPage > gTotalPages && gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadProductDataToTable();
        } else {
            gPage = vPage - 1;
            loadProductDataToTable();
        }
    }

    function onChangePageSize() {
        gPage = 0;
        gPageSize = $("#sel-page-size").val();
        loadProductDataToTable();
    }

    //Hàm xử lý khi ấn nút Trang đầu
    function onFirstPageClick() {
        if (gTotalPages > 0) {
            gPage = 0;
            loadProductDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang trước
    function onPreviousPageClick() {
        if (gTotalPages > 0 && gPage > 0) {
            gPage--;
            loadProductDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang sau
    function onNextPageClick() {
        if (gTotalPages > 0 && gPage < gTotalPages - 1) {
            gPage++;
            loadProductDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang cuối
    function onLastPageClick() {
        if (gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadProductDataToTable();
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm load dữ liệu Product vào bảng
    function loadProductDataToTable() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "products?keyword=" + gKeyword + "&productBrandId=" + gProductBrandId + "&processorBrandId=" + gProcessorBrandId + "&enabled=" + gEnabled + "&sortBy=" + gSortBy + "&page=" + gPage + "&size=" + gPageSize,
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    gDataTable.clear();
                    gDataTable.rows.add(res.content);
                    gDataTable.draw();
                    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
                    gTotalPages = res.totalPages;
                    $("#number-page").val(gPage + 1);
                    $("#element-first").html(gPage * gPageSize + (res.numberOfElements !== 0 ? 1 : 0));
                    $("#element-last").html(gPage * gPageSize + res.numberOfElements);
                    $("#total-elements").html(res.totalElements);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm load dữ liệu vào Select Product Brand
    function loadDataToSelectProductBrand(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-productBrand").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Processor Brand
    function loadDataToSelectProcessorBrand(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-processorBrand").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Graphics Brand
    function loadDataToSelectGraphicsBrand(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-graphicsBrand").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm xử lý hiển thị chi tiết
    function loadDetailData(paramData) {
        "use strict";
        $("#p-id").html(paramData.id);
        $("#p-productCode").html(paramData.productCode);
        $("#p-productName").html(paramData.productName);
        if (paramData.productBrand != null) {
            $("#p-productBrand").html(paramData.productBrand.name);
        } else {
            $("#p-productBrand").html("");
        }
        if (paramData.productLine != null) {
            $("#p-productLine").html(paramData.productLine.name);
        } else {
            $("#p-productLine").html("");
        }
        $("#p-productDescription").html(paramData.productDescription);
        $("#p-releaseDate").html(paramData.releaseDate);
        $("#p-buyPrice").html(paramData.buyPrice);
        $("#p-discountPercentage").html(paramData.discountPercentage);
        $("#p-color").html(paramData.color);
        var vProductPhotos = paramData.productPhotos;
        $("#carousel-images").html("");
        $("#carouselControls").prop("hidden", true);
        if (vProductPhotos != null && vProductPhotos.length > 0) {
            $("#carousel-images").append(
                "<div class='carousel-item active'>"
                + "<img src='" + gBASE_URL + "product-photos/" + vProductPhotos[0].name + "' class='d-block w-100' style='height: 500px; object-fit: contain;' alt='...'>"
                + "</div>"
            );
            for (var bI = 1; bI < vProductPhotos.length; bI++) {
                $("#carousel-images").append(
                    "<div class='carousel-item'>"
                    + "<img src='" + gBASE_URL + "product-photos/" + vProductPhotos[bI].name + "' class='d-block w-100' style='height: 500px; object-fit: contain;' alt='...'>"
                    + "</div>"
                );
            }
            if (vProductPhotos.length > 1) {
                $("#btn-carousel-control-prev").prop("hidden", false);
                $("#btn-carousel-control-next").prop("hidden", false);
            } else {
                $("#btn-carousel-control-prev").prop("hidden", true);
                $("#btn-carousel-control-next").prop("hidden", true);
            }
            $("#carouselControls").prop("hidden", false);
        } else {
            $("#carousel-images").html("");
        }
        var vProcessor = paramData.processor;
        if (vProcessor != null) {
            $("#p-processor").html(vProcessor.fullName);
            $("#p-totalCores").html(vProcessor.totalCores);
            $("#p-totalThreads").html(vProcessor.totalThreads);
            if (vProcessor.baseFrequency != null) {
                $("#p-baseFrequency").html(vProcessor.baseFrequency.toFixed(2));
            } else {
                $("#p-baseFrequency").html("");
            }
            if (vProcessor.maxTurboFrequency != null) {
                $("#p-maxTurboFrequency").html(vProcessor.maxTurboFrequency.toFixed(2));
            } else {
                $("#p-maxTurboFrequency").html("");
            }
            $("#p-cache").html(vProcessor.cache);
            $("#p-processorGraphics").html(vProcessor.processorGraphics);
            if (vProcessor.graphicsFrequency != null) {
                $("#p-graphicsFrequency").html(vProcessor.graphicsFrequency.toFixed(2));
            } else {
                $("#p-graphicsFrequency").html("");
            }
        } else {
            $("#p-processor").html("");
            $("#p-totalCores").html("");
            $("#p-totalThreads").html("");
            $("#p-cache").html("");
            $("#p-processorGraphics").html("");
        }
        var vGraphics = paramData.graphics;
        if (vGraphics != null) {
            $("#p-graphics").html(vGraphics.fullName);
            $("#p-gpuMemory").html(vGraphics.gpuMemory);
        } else {
            $("#p-graphics").html("");
            $("#p-gpuMemory").html("");
        }
        $("#p-storageType").html(paramData.storageType);
        $("#p-totalStorageCapacity").html(paramData.totalStorageCapacity);
        $("#p-ssdType").html(paramData.ssdType);
        $("#p-systemMemory").html(paramData.systemMemory);
        $("#p-typeOfMemory").html(paramData.typeOfMemory);
        $("#p-systemMemorySpeed").html(paramData.systemMemorySpeed);
        $("#p-numberOfMemorySlots").html(paramData.numberOfMemorySlots);
        $("#p-numberOfMemorySticksIncluded").html(paramData.numberOfMemorySticksIncluded);
        $("#p-screenType").html(paramData.screenType);
        $("#p-displayType").html(paramData.displayType);
        $("#p-screenSize").html(paramData.screenSize);
        $("#p-screenResolutionX").html(paramData.screenResolutionX);
        $("#p-screenResolutionY").html(paramData.screenResolutionY);
        $("#p-touchScreen").html(stringYesOrNo(paramData.touchScreen));
        $("#p-wifi").html(paramData.wifi);
        $("#p-bluetooth").html(paramData.bluetooth);
        $("#p-numberOfUSBPorts").html(paramData.numberOfUSBPorts);
        $("#p-headphoneJack").html(stringYesOrNo(paramData.headphoneJack));
        $("#p-productWidth").html(paramData.productWidth);
        $("#p-productHeight").html(paramData.productHeight);
        $("#p-productDepth").html(paramData.productDepth);
        $("#p-productWeight").html(paramData.productWeight);
        $("#p-batteryType").html(paramData.batteryType);
        $("#p-batteryLife").html(paramData.batteryLife);
        $("#p-batteryCells").html(paramData.batteryCells);
        $("#p-powerSupply").html(paramData.powerSupply);
        $("#p-operatingSystem").html(paramData.operatingSystem);
        $("#p-frontFacingCamera").html(stringYesOrNo(paramData.frontFacingCamera));
        $("#p-numericKeyboard").html(stringYesOrNo(paramData.numericKeyboard));
        $("#p-backlitKeyboard").html(stringYesOrNo(paramData.backlitKeyboard));
        $("#p-warranty").html(paramData.warranty);
        $("#p-quantityInStock").html(paramData.quantityInStock);
        $("#p-createdAt").html(paramData.createdAt);
        $("#p-updatedAt").html(paramData.updatedAt);
        $("#p-createdBy").html(paramData.createdBy);
        $("#p-updatedBy").html(paramData.updatedBy);
        
        if (paramData.deleted) {
            $("#btn-enable-disable").html("Enable");
            $("#modal-enable-disable .enable-product").prop("hidden", false);
            $("#modal-enable-disable .disable-product").prop("hidden", true);
            $("#btn-enable-disable-confirm").html("Enable");
        } else {
            $("#btn-enable-disable").html("Disable");
            $("#modal-enable-disable .enable-product").prop("hidden", true);
            $("#modal-enable-disable .disable-product").prop("hidden", false);
            $("#btn-enable-disable-confirm").html("Disable");
        }
    }

    function stringYesOrNo(paramCheck) {
        if (paramCheck) {
            return "Yes";
        }
        return "No";
    }
});