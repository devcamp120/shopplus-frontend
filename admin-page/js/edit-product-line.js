$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";
    var gName = "";

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện click cho nút Save ProductLine
    Array.from($("#form-edit")).forEach(form => {
        form.addEventListener("submit", event => {
            event.preventDefault();
            event.stopPropagation();
            if (form.checkValidity()) {
                onBtnUpdateClick();
            }
            form.classList.add("was-validated");
        }, false);
    });

    //Gán sự kiện cho nút Cancel
    $("#btn-cancel").on("click", function () {
        window.history.back();
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        const vUrlParams = new URLSearchParams(window.location.search);
        gId = vUrlParams.get("id");
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "product-brands/all",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    loadDataToSelectProductBrand(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });

            if (gId != "" && gId != null) {
                $.ajax({
                    url: gBASE_URL + "product-lines/" + gId,
                    method: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    success: function (res) {
                        loadEditData(res);
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi ấn nút Update
    function onBtnUpdateClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vProductLine = {
                productBrand: "",
                name: "",
                description: "",
            }
            //Lấy dữ liệu
            getProductLineData(vProductLine);
            //Kiểm tra dữ liệu
            var vIsCheck = gName == vProductLine.name ? true : !checkName(vProductLine.name);
            if (vIsCheck) {
                $.ajax({
                    url: gBASE_URL + "product-lines/" + gId,
                    method: "PUT",
                    async: false,
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vProductLine),
                    success: function (res) {
                        window.location.href = "product-lines.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            } else {
                toastr.warning("Name already exists!");
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm load dữ liệu vào Select Product Brand
    function loadDataToSelectProductBrand(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-productBrand").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm xử lý hiển thị dữ liệu Product Line
    function loadEditData(paramData) {
        "use strict";
        if (paramData.productBrand != null) {
            $("#sel-productBrand").val(paramData.productBrand.id).trigger("change");
        } else {
            $("#sel-productBrand").val("").trigger("change");
        }
        gName = paramData.name;
        $("#inp-name").val(gName);
        $("#inp-description").val(paramData.description);
    }

    //Hàm lấy dữ liệu Product Line
    function getProductLineData(paramData) {
        paramData.productBrand = {
            id: $("#sel-productBrand").val().trim()
        }
        paramData.name = $("#inp-name").val().trim();
        paramData.description = $("#inp-description").val().trim();
    }

    //Hàm kiểm tra name
    function checkName(paramName) {
        "use strict";
        var vIsCheck = true;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "product-lines/name/" + paramName + "/exists",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    vIsCheck = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
        return vIsCheck;
    }
});