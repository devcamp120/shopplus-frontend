$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";
    var gProductCode = "";
    var gDeletePhotoIds = [];

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho Select Product Brand
    $("#sel-productBrand").on("change", onSelectProductBrandChange);

    //Gán sự kiện click cho nút Update
    Array.from($("#form-edit")).forEach(form => {
        form.addEventListener("submit", event => {
            event.preventDefault();
            event.stopPropagation();
            if (form.checkValidity()) {
                onBtnUpdateClick();
            } else {
                var listInvalid = form.querySelectorAll(":invalid");
                var idInvalid = listInvalid[0].closest(".tab-pane").id;
                $("#nav-tab button[data-bs-target='#" + idInvalid + "']").tab("show");
            }
            form.classList.add("was-validated");
        }, false);
    });

    //Gán sự kiện cho nút Cancel
    $("#btn-cancel").on("click", function () {
        window.history.back();
    });

    //Gán sự kiện cho nút Add Processor
    $("#add-processor").on("click", function () {
        window.open("add-processor.html", "_blank");
    });

    //Gán sự kiện cho nút Add Graphics
    $("#add-graphics").on("click", function () {
        window.open("add-graphics.html", "_blank");
    });

    $("#card-images").on("click", ".delete-server-image", function () {
        var vPhotoId = $(this)[0].dataset.photoId;
        gDeletePhotoIds.push(vPhotoId);
        $(this).parent().parent().remove();
    });

    $("#card-images").on("click", ".delete-local-image", function () {
        $("#file-photo").val("");
        $("#card-images .delete-local-image").parent().parent().remove();
    });

    $("#file-photos").on("change", function () {
        $("#card-images .delete-local-image").parent().parent().remove();
        var vFiles = this.files;
        for (var bI = 0; bI < vFiles.length; bI++) {
            if (vFiles[bI]) {
                let reader = new FileReader();
                reader.onload = function (event) {
                    $("#card-images").append(
                        "<div class='col'>"
                        + "<div class='card border-0'>"
                        + "<img src='" + event.target.result + "' class='card-img-top' style='object-fit: contain;' alt='...'>"
                        + "<button class='btn border-0 delete-local-image' style='position: absolute; top: 0%; left: 85%;'><i class='text-danger fas fa-window-close'></i></button>"
                        + "</div>"
                        + "</div>"
                    );
                }
                reader.readAsDataURL(vFiles[bI]);
            }
        }
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        const vUrlParams = new URLSearchParams(window.location.search);
        gId = vUrlParams.get("id");
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "product-brands/all",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    loadDataToSelectProductBrand(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });

            $("#sel-processor").select2({
                theme: "bootstrap-5",
                placeholder: "Enter Processor Number",
                minimumInputLength: 2,
                allowClear: true,
                ajax: {
                    url: gBASE_URL + "processors/select-search",
                    type: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    dataType: "json",
                    delay: 250,
                    data: function (params) {
                        return {
                            keyword: params.term
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (processor) {
                                return {
                                    text: processor.fullName,
                                    id: processor.id,
                                    data: processor
                                };
                            })
                        };
                    }
                }
            });

            $("#sel-graphics").select2({
                theme: "bootstrap-5",
                placeholder: "Enter GPU Name",
                minimumInputLength: 2,
                allowClear: true,
                ajax: {
                    url: gBASE_URL + "graphics/select-search",
                    type: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    dataType: "json",
                    delay: 250,
                    data: function (params) {
                        return {
                            keyword: params.term
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (graphics) {
                                return {
                                    text: graphics.fullName,
                                    id: graphics.id,
                                    data: graphics
                                };
                            })
                        };
                    }
                }
            });

            if (gId != "" && gId != null) {
                $.ajax({
                    url: gBASE_URL + "products/" + gId,
                    method: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    success: function (res) {
                        loadEditData(res);
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi ấn nút Update
    function onBtnUpdateClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vProduct = {
                productCode: "",
                productBrand: "",
                productLine: "",
                productName: "",
                productDescription: "",
                releaseDate: "",
                buyPrice: "",
                discountPercentage: "",
                color: "",
                processorBrand: "",
                processorCollection: "",
                processor: "",
                storageType: "",
                totalStorageCapacity: "",
                ssdType: "",
                systemMemory: "",
                typeOfMemory: "",
                systemMemorySpeed: "",
                numberOfMemorySlots: "",
                numberOfMemorySticksIncluded: "",
                screenType: "",
                displayType: "",
                screenSize: "",
                screenResolutionX: "",
                screenResolutionY: "",
                touchScreen: "",
                graphicsBrand: "",
                graphicsType: "",
                graphics: "",
                wifi: "",
                bluetooth: "",
                headphoneJack: "",
                numberOfUSBPorts: "",
                batteryType: "",
                batteryLife: "",
                batteryCells: "",
                powerSupply: "",
                operatingSystem: "",
                frontFacingCamera: "",
                numericKeyboard: "",
                backlitKeyboard: "",
                productWidth: "",
                productHeight: "",
                productDepth: "",
                productWeight: "",
                warranty: "",
                quantityInStock: ""
            }
            //Lấy dữ liệu
            getProductData(vProduct);
            //Kiểm tra dữ liệu
            var vIsCheck = gProductCode == vProduct.productCode ? true : !checkProductCode(vProduct.productCode);
            if (vIsCheck) {
                $.ajax({
                    url: gBASE_URL + "products/" + gId,
                    method: "PUT",
                    async: false,
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vProduct),
                    success: function (res) {
                        if (gDeletePhotoIds.length > 0) {
                            $.ajax({
                                url: gBASE_URL + "product-photos",
                                method: "DELETE",
                                async: false,
                                headers: {
                                    Authorization: "Bearer " + token,
                                    "Content-Type": "application/json"
                                },
                                data: JSON.stringify(gDeletePhotoIds),
                                success: function (res) {

                                },
                                error: function (err) {
                                    console.log(err.responseText);
                                }
                            });
                        }
                        if ($("#file-photos")[0].files.length > 0) {
                            var vForm = getPhotoInputData();
                            $.ajax({
                                url: gBASE_URL + "product-photos?productId=" + res.id,
                                method: "POST",
                                headers: {
                                    Authorization: "Bearer " + token
                                },
                                async: false,
                                processData: false,
                                mimeType: "multipart/form-data",
                                contentType: false,
                                data: vForm,
                                success: function (res) {
                                    window.location.href = "products.html";
                                },
                                error: function (err) {
                                    console.log(err.responseText);
                                }
                            });
                        } else {
                            window.location.href = "products.html";
                        }
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            } else {
                toastr.warning("Product Code already exists!");
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi chọn Product Brand
    function onSelectProductBrandChange() {
        var vProductBrandId = $("#sel-productBrand").val();
        $("#sel-productLine").html("");
        $("#sel-productLine").append($("<option>").val("").text("Product Line"));
        if (vProductBrandId != "") {
            var token = getCookie("token");
            if (token) {
                $.ajax({
                    url: gBASE_URL + "product-brands/" + vProductBrandId + "/product-lines",
                    method: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    async: false,
                    success: function (res) {
                        $("#sel-productLine").prop("disabled", false);
                        loadDataToSelectProductLine(res);
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            } else {
                toastr.error("You are not logged in!");
            }
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm xử lý hiển thị dữ liệu Product
    function loadEditData(paramData) {
        "use strict";
        gProductCode = paramData.productCode;
        $("#inp-productCode").val(gProductCode);
        $("#sel-productBrand").val(paramData.productBrand.id).trigger("change");
        $("#sel-productLine").val(paramData.productLine.id).trigger("change");
        $("#inp-productName").val(paramData.productName);
        $("#inp-productDescription").val(paramData.productDescription);
        $("#inp-releaseDate").val(paramData.releaseDate);
        $("#inp-buyPrice").val(paramData.buyPrice);
        $("#inp-discountPercentage").val(paramData.discountPercentage);
        $("#inp-color").val(paramData.color);
        var vProductPhotos = paramData.productPhotos;
        if (vProductPhotos != null && vProductPhotos.length > 0) {
            for (var bI = 0; bI < vProductPhotos.length; bI++) {
                $("#card-images").append(
                    "<div class='col'>"
                    + "<div class='card border-0'>"
                    + "<img src='" + gBASE_URL + "product-photos/" + vProductPhotos[bI].name + "' class='card-img-top' style='object-fit: contain;' alt='...'>"
                    + "<button data-photo-id='" + vProductPhotos[bI].id + "' class='btn border-0 delete-server-image' style='position: absolute; top: 0%; left: 80%;'><i class='text-danger fas fa-window-close'></i></button>"
                    + "</div>"
                    + "</div>"
                );
            }
        }
        var vProcessor = paramData.processor;
        if (vProcessor != null) {
            var vText = "";
            if (vProcessor.processorCollection == null) {
                vText = vProcessor.processorBrand.name + " " + vProcessor.processorNumber;
            } else {
                vText = vProcessor.processorBrand.name + " " + vProcessor.processorCollection.name + " " + vProcessor.processorNumber;
            }
            var newOption = new Option(vText, vProcessor.id, true, true);
            $("#sel-processor").append(newOption).trigger("change");
        }
        var vGraphics = paramData.graphics;
        if (vGraphics != null) {
            var vText = "";
            if (vGraphics.graphicsType == null) {
                vText = vGraphics.graphicsBrand.name + " " + vGraphics.gpuName;
            } else {
                vText = vGraphics.graphicsBrand.name + " " + vGraphics.graphicsType.name + " " + vGraphics.gpuName;
            }
            var newOption = new Option(vText, vGraphics.id, true, true);
            $("#sel-graphics").append(newOption).trigger("change");
        }
        $("#inp-storageType").val(paramData.storageType);
        $("#inp-totalStorageCapacity").val(paramData.totalStorageCapacity);
        $("#inp-ssdType").val(paramData.ssdType);
        $("#inp-systemMemory").val(paramData.systemMemory);
        $("#inp-typeOfMemory").val(paramData.typeOfMemory);
        $("#inp-systemMemorySpeed").val(paramData.systemMemorySpeed);
        $("#inp-numberOfMemorySlots").val(paramData.numberOfMemorySlots);
        $("#inp-numberOfMemorySticksIncluded").val(paramData.numberOfMemorySticksIncluded);
        $("#inp-screenType").val(paramData.screenType);
        $("#inp-displayType").val(paramData.displayType);
        $("#inp-screenSize").val(paramData.screenSize);
        $("#inp-screenResolutionX").val(paramData.screenResolutionX);
        $("#inp-screenResolutionY").val(paramData.screenResolutionY);
        if (paramData.touchScreen != null) {
            $("#cbx-touchScreen").prop("checked", paramData.touchScreen);
        }
        $("#inp-wifi").val(paramData.wifi);
        $("#inp-bluetooth").val(paramData.bluetooth);
        $("#inp-numberOfUSBPorts").val(paramData.numberOfUSBPorts);
        if (paramData.headphoneJack != null) {
            $("#cbx-headphoneJack").prop("checked", paramData.headphoneJack);
        }
        $("#inp-productWidth").val(paramData.productWidth);
        $("#inp-productHeight").val(paramData.productHeight);
        $("#inp-productDepth").val(paramData.productDepth);
        $("#inp-productWeight").val(paramData.productWeight);
        $("#inp-batteryType").val(paramData.batteryType);
        $("#inp-batteryLife").val(paramData.batteryLife);
        $("#inp-batteryCells").val(paramData.batteryCells);
        $("#inp-powerSupply").val(paramData.powerSupply);
        $("#inp-operatingSystem").val(paramData.operatingSystem);
        if (paramData.frontFacingCamera != null) {
            $("#cbx-frontFacingCamera").prop("checked", paramData.frontFacingCamera);
        }
        if (paramData.numericKeyboard != null) {
            $("#cbx-numericKeyboard").prop("checked", paramData.numericKeyboard);
        }
        if (paramData.backlitKeyboard != null) {
            $("#cbx-backlitKeyboard").prop("checked", paramData.backlitKeyboard);
        }
        $("#inp-warranty").val(paramData.warranty);
        $("#inp-quantityInStock").val(paramData.quantityInStock);
    }

    //Hàm load dữ liệu vào Select Product Brand
    function loadDataToSelectProductBrand(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-productBrand").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Product Line
    function loadDataToSelectProductLine(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-productLine").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm lấy dữ liệu hình ảnh để upload
    function getPhotoInputData() {
        var formData = new FormData();
        var vFiles = $("#file-photos")[0].files;
        for (var bI = 0; bI < vFiles.length; bI++) {
            formData.append("files", vFiles[bI]);
        }
        return formData;
    }

    //Hàm lấy dữ liệu Product
    function getProductData(paramData) {
        paramData.productCode = $("#inp-productCode").val().trim();
        paramData.productBrand = {
            id: $("#sel-productBrand").val()
        }
        var vProductLineId = $("#sel-productLine").val();
        if (vProductLineId != "" && vProductLineId != null) {
            paramData.productLine = {
                id: vProductLineId
            }
        } else {
            paramData.productLine = null;
        }
        paramData.productName = $("#inp-productName").val().trim();
        paramData.productDescription = $("#inp-productDescription").val().trim();
        paramData.releaseDate = $("#inp-releaseDate").val().trim();
        paramData.buyPrice = $("#inp-buyPrice").val().trim();
        paramData.discountPercentage = $("#inp-discountPercentage").val().trim();
        paramData.color = $("#inp-color").val().trim();
        paramData.processor = {
            id: $("#sel-processor").val()
        }
        var vGraphicsId = $("#sel-graphics").val();
        if (vGraphicsId != "" && vGraphicsId != null) {
            paramData.graphics = {
                id: vGraphicsId
            }
        } else {
            paramData.graphics = null;
        }
        paramData.storageType = $("#inp-storageType").val().trim();
        paramData.totalStorageCapacity = $("#inp-totalStorageCapacity").val().trim();
        paramData.ssdType = $("#inp-ssdType").val().trim();
        paramData.systemMemory = $("#inp-systemMemory").val().trim();
        paramData.typeOfMemory = $("#inp-typeOfMemory").val().trim();
        paramData.systemMemorySpeed = $("#inp-systemMemorySpeed").val().trim();
        paramData.numberOfMemorySlots = $("#inp-numberOfMemorySlots").val().trim();
        paramData.numberOfMemorySticksIncluded = $("#inp-numberOfMemorySticksIncluded").val().trim();
        paramData.screenType = $("#inp-screenType").val().trim();
        paramData.displayType = $("#inp-displayType").val().trim();
        paramData.screenSize = $("#inp-screenSize").val().trim();
        paramData.screenResolutionX = $("#inp-screenResolutionX").val().trim();
        paramData.screenResolutionY = $("#inp-screenResolutionY").val().trim();
        paramData.touchScreen = $("#cbx-touchScreen").is(":checked");
        paramData.wifi = $("#inp-wifi").val().trim();
        paramData.bluetooth = $("#inp-bluetooth").val().trim();
        paramData.headphoneJack = $("#cbx-headphoneJack").is(":checked");
        paramData.numberOfUSBPorts = $("#inp-numberOfUSBPorts").val().trim();
        paramData.batteryType = $("#inp-batteryType").val().trim();
        paramData.batteryLife = $("#inp-batteryLife").val().trim();
        paramData.batteryCells = $("#inp-batteryCells").val().trim();
        paramData.powerSupply = $("#inp-powerSupply").val().trim();
        paramData.operatingSystem = $("#inp-operatingSystem").val().trim();
        paramData.frontFacingCamera = $("#cbx-frontFacingCamera").is(":checked");
        paramData.numericKeyboard = $("#cbx-numericKeyboard").is(":checked");
        paramData.backlitKeyboard = $("#cbx-backlitKeyboard").is(":checked");
        paramData.productWidth = $("#inp-productWidth").val().trim();
        paramData.productHeight = $("#inp-productHeight").val().trim();
        paramData.productDepth = $("#inp-productDepth").val().trim();
        paramData.productWeight = $("#inp-productWeight").val().trim();
        paramData.warranty = $("#inp-warranty").val().trim();
        paramData.quantityInStock = $("#inp-quantityInStock").val().trim();
    }

    //Hàm kiểm tra productCode
    function checkProductCode(paramProductCode) {
        "use strict";
        var vIsCheck = true;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "products/productCode/" + paramProductCode + "/exists",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    vIsCheck = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
        return vIsCheck;
    }
});