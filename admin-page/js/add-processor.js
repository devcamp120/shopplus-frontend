$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    $("#sel-processorBrand").on("change", onSelectProcessorBrandChange);

    //Gán sự kiện click cho nút Add
    Array.from($("#form-add")).forEach(form => {
        form.addEventListener("submit", event => {
            event.preventDefault();
            event.stopPropagation();
            if (form.checkValidity()) {
                onBtnAddClick();
            }
            form.classList.add("was-validated");
        }, false);
    });

    //Gán sự kiện cho nút Cancel
    $("#btn-cancel").on("click", function () {
        window.history.back();
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "processor-brands/all",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    loadDataToSelectProcessorBrand(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi ấn nút Add
    function onBtnAddClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vProcessor = {
                processorBrand: "",
                processorCollection: "",
                processorNumber: "",
                totalCores: "",
                totalThreads: "",
                baseFrequency: "",
                maxTurboFrequency: "",
                cache: "",
                processorGraphics: "",
                graphicsFrequency: ""
            }
            //Lấy dữ liệu
            getProcessorData(vProcessor);
            //Kiểm tra dữ liệu
            var vIsCheck = !checkProcessorNumber(vProcessor.processorNumber);

            if (vIsCheck) {
                $.ajax({
                    url: gBASE_URL + "processors",
                    method: "POST",
                    async: false,
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vProcessor),
                    success: function (res) {
                        window.location.href = "processors.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            } else {
                toastr.warning("Processor Number already exists!");
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi chọn Processor Brand
    function onSelectProcessorBrandChange() {
        var vProcessorBrandId = $("#sel-processorBrand").val();
        $("#sel-processorCollection").html("");
        $("#sel-processorCollection").append($("<option>").val("").text("Processor Collection"));
        if (vProcessorBrandId != "") {
            var token = getCookie("token");
            if (token) {
                $.ajax({
                    url: gBASE_URL + "processor-brands/" + vProcessorBrandId + "/processor-collections",
                    method: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    async: false,
                    success: function (res) {
                        $("#sel-processorCollection").prop("disabled", false);
                        loadDataToSelectProcessorCollection(res);
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            } else {
                toastr.error("You are not logged in!");
            }
        } else {
            $("#sel-processorCollection").prop("disabled", true);
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm load dữ liệu vào Select Processor Brand
    function loadDataToSelectProcessorBrand(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-processorBrand").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Processor Collection
    function loadDataToSelectProcessorCollection(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-processorCollection").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm lấy dữ liệu Processor
    function getProcessorData(paramData) {
        paramData.processorBrand = {
            id: $("#sel-processorBrand").val().trim()
        }
        var vProcessorCollectionId = $("#sel-processorCollection").val().trim();
        if (vProcessorCollectionId != "") {
            paramData.processorCollection = {
                id: vProcessorCollectionId
            }
        } else {
            paramData.processorCollection = null;
        }
        paramData.processorNumber = $("#inp-processorNumber").val().trim();
        paramData.totalCores = $("#inp-totalCores").val().trim();
        paramData.totalThreads = $("#inp-totalThreads").val().trim();
        paramData.baseFrequency = $("#inp-baseFrequency").val().trim();
        paramData.maxTurboFrequency = $("#inp-maxTurboFrequency").val().trim();
        paramData.cache = $("#inp-cache").val().trim();
        paramData.processorGraphics = $("#inp-processorGraphics").val().trim();
        paramData.graphicsFrequency = $("#inp-graphicsFrequency").val().trim();
    }

    //Hàm kiểm tra processorNumber
    function checkProcessorNumber(paramProcessorNumber) {
        "use strict";
        var vIsCheck = true;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "processors/processor-number/" + paramProcessorNumber + "/exists",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    vIsCheck = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
        return vIsCheck;
    }
});