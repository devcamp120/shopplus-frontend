$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện click cho nút Save Payment
    Array.from($("#form-edit")).forEach(form => {
        form.addEventListener("submit", event => {
            event.preventDefault();
            event.stopPropagation();
            if (form.checkValidity()) {
                onBtnUpdateClick();
            }
            form.classList.add("was-validated");
        }, false);
    });

    //Gán sự kiện cho nút Cancel
    $("#btn-cancel").on("click", function () {
        window.history.back();
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        const vUrlParams = new URLSearchParams(window.location.search);
        gId = vUrlParams.get("id");
        var token = getCookie("token");
        if (token) {
            if (gId != "" && gId != null) {
                $.ajax({
                    url: gBASE_URL + "payments/" + gId,
                    method: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    success: function (res) {
                        loadEditData(res);
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }

            $("#sel-customer").select2({
                theme: "bootstrap-5",
                placeholder: "Enter Name / Phone Number",
                minimumInputLength: 2,
                allowClear: true,
                ajax: {
                    url: gBASE_URL + "users/select-search",
                    type: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    dataType: "json",
                    delay: 250,
                    data: function (params) {
                        return {
                            keyword: params.term
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (user) {
                                return {
                                    text: user.firstName + " " + user.lastName + " - " + user.username,
                                    id: user.id,
                                    data: user
                                };
                            })
                        };
                    }
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi ấn nút Update
    function onBtnUpdateClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vPayment = {
                user: "",
                ammount: "",
            }
            //Lấy dữ liệu
            getPaymentData(vPayment);

            $.ajax({
                url: gBASE_URL + "payments/" + gId,
                method: "PUT",
                async: false,
                headers: {
                    Authorization: "Bearer " + token,
                    "Content-Type": "application/json"
                },
                data: JSON.stringify(vPayment),
                success: function (res) {
                    window.location.href = "payments.html";
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm xử lý hiển thị dữ liệu Payment
    function loadEditData(paramData) {
        "use strict";
        var vCustomer = paramData.user;
        if (vCustomer != null) {
            var vText = vCustomer.firstName + " " + vCustomer.lastName + " - " + vCustomer.username;
            var newOption = new Option(vText, vCustomer.id, true, true);
            $("#sel-customer").append(newOption).trigger("change");
        }
        $("#inp-ammount").val(paramData.ammount);
    }

    //Hàm lấy dữ liệu Payment
    function getPaymentData(paramData) {
        paramData.user = {
            id: $("#sel-customer").val()
        }
        paramData.ammount = $("#inp-ammount").val().trim();
    }
});