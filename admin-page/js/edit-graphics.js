$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";
    var gGpuName = "";

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho Select Graphics Brand
    $("#sel-graphicsBrand").on("change", onSelectGraphicsBrandChange);

    //Gán sự kiện click cho nút Save Graphics
    Array.from($("#form-edit")).forEach(form => {
        form.addEventListener("submit", event => {
            event.preventDefault();
            event.stopPropagation();
            if (form.checkValidity()) {
                onBtnUpdateClick();
            }
            form.classList.add("was-validated");
        }, false);
    });

    //Gán sự kiện cho nút Cancel
    $("#btn-cancel").on("click", function () {
        window.history.back();
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        const vUrlParams = new URLSearchParams(window.location.search);
        gId = vUrlParams.get("id");
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "graphics-brands/all",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    loadDataToSelectGraphicsBrand(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });

            if (gId != "" && gId != null) {
                $.ajax({
                    url: gBASE_URL + "graphics/" + gId,
                    method: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    success: function (res) {
                        loadEditData(res);
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi ấn nút Update
    function onBtnUpdateClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vGraphics = {
                graphicsBrand: "",
                graphicsType: "",
                gpuName: "",
                gpuMemory: ""
            }
            //Lấy dữ liệu
            getGraphicsData(vGraphics);
            //Kiểm tra dữ liệu
            var vIsCheck = gGpuName == vGraphics.gpuName ? true : !checkGpuName(vGraphics.gpuName);
            if (vIsCheck) {
                $.ajax({
                    url: gBASE_URL + "graphics/" + gId,
                    method: "PUT",
                    async: false,
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vGraphics),
                    success: function (res) {
                        window.location.href = "graphics.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            } else {
                toastr.warning("GPU Name already exists!");
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi chọn Graphics Brand
    function onSelectGraphicsBrandChange() {
        var vGraphicsBrandId = $("#sel-graphicsBrand").val();
        $("#sel-graphicsType").html("");
        $("#sel-graphicsType").append($("<option>").val("").text("Graphics Type"));
        if (vGraphicsBrandId != "") {
            var token = getCookie("token");
            if (token) {
                $.ajax({
                    url: gBASE_URL + "graphics-brands/" + vGraphicsBrandId + "/graphics-types",
                    method: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    async: false,
                    success: function (res) {
                        $("#sel-graphicsType").prop("disabled", false);
                        loadDataToSelectGraphicsType(res);
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            } else {
                toastr.error("You are not logged in!");
            }
        } else {
            $("#sel-graphicsType").prop("disabled", true);
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm load dữ liệu vào Select Graphics Brand
    function loadDataToSelectGraphicsBrand(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-graphicsBrand").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm load dữ liệu vào Select Graphics Type
    function loadDataToSelectGraphicsType(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-graphicsType").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm xử lý hiển thị dữ liệu Graphics
    function loadEditData(paramData) {
        "use strict";
        if (paramData.graphicsBrand != null) {
            $("#sel-graphicsBrand").val(paramData.graphicsBrand.id).trigger("change");
        } else {
            $("#sel-graphicsBrand").val("").trigger("change");
        }
        if (paramData.graphicsType != null) {
            $("#sel-graphicsType").val(paramData.graphicsType.id).trigger("change");
        } else {
            $("#sel-graphicsType").val("").trigger("change");
        }
        gGpuName = paramData.gpuName
        $("#inp-gpuName").val(gGpuName);
        $("#inp-gpuMemory").val(paramData.gpuMemory);
    }

    //Hàm lấy dữ liệu Graphics
    function getGraphicsData(paramData) {
        paramData.graphicsBrand = {
            id: $("#sel-graphicsBrand").val().trim()
        }
        var vGraphicsTypeId = $("#sel-graphicsType").val().trim();
        if (vGraphicsTypeId != "") {
            paramData.graphicsType = {
                id: vGraphicsTypeId
            }
        } else {
            paramData.graphicsType = null;
        }
        paramData.gpuName = $("#inp-gpuName").val().trim();
        paramData.gpuMemory = $("#inp-gpuMemory").val().trim();
    }

    //Hàm kiểm tra gpuName
    function checkGpuName(paramGpuName) {
        "use strict";
        var vIsCheck = true;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "graphics/gpu-name/" + paramGpuName + "/exists",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    vIsCheck = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
        return vIsCheck;
    }
});