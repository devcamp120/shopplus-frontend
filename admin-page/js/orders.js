"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gTotalPages = 0;
    var gPage = 0;
    var gPageSize = 10;
    var gId = "";
    const gStartDate = "2021-01-01";
    const gEndOrderDate = moment().format("YYYY-MM-DD");
    const gEndRequiredDate = "2050-01-01";
    var gKeyword = "";
    var gSortBy = "0";
    var gCustomerId = "-1";
    var gOrderDate = [gStartDate, gEndOrderDate];
    var gRequiredDate = [gStartDate, gEndRequiredDate];
    var gStatus = "All";
    const gNameCol = ["action", "id", "orderCode", "user", "orderDate", "requiredDate", "shippedDate", "status", "totalOrderAmount"];
    const gACTION_COL = 0;
    const gID_COL = 1;
    const gORDER_CODE_COL = 2;
    const gCUSTOMER_COL = 3;
    const gORDER_DATE_COL = 4;
    const gREQUIRED_DATE_COL = 5;
    const gSHIPPED_DATE_COL = 6;
    const gSTATUS_COL = 7;
    const gTOTAL_ORDER_AMOUNT = 8;
    var gDataTable = $("#table-orders").DataTable({
        columns: [
            { data: gNameCol[gACTION_COL] },
            { data: gNameCol[gID_COL] },
            { data: gNameCol[gORDER_CODE_COL] },
            { data: gNameCol[gCUSTOMER_COL] },
            { data: gNameCol[gORDER_DATE_COL] },
            { data: gNameCol[gREQUIRED_DATE_COL] },
            { data: gNameCol[gSHIPPED_DATE_COL] },
            { data: gNameCol[gSTATUS_COL] },
            { data: gNameCol[gTOTAL_ORDER_AMOUNT] }
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: "<a href='javascript:void(0)' class='dropdown'><a href='javascript:void(0)' class='dropdown-toggle text-dark me-3' data-bs-toggle='dropdown' aria-expanded='false'><i class='fa-solid fa-ellipsis'></i></a><ul class='dropdown-menu'><li><a class='dropdown-item item-edit' href='javascript:void(0)'>Edit</a></li><li><a class='dropdown-item item-delete' href='javascript:void(0)'>Delete</a></li></ul></a><a href='javascript:void(0)' class='item-detail' title='Detail'><i class='text-info fa-solid fa-circle-info'></i></a>",
                className: "text-nowrap"
            },
            {
                targets: gCUSTOMER_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.firstName + " " + data.lastName + " - " + data.username;
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gORDER_DATE_COL,
                className: "text-nowrap"
            },
            {
                targets: gREQUIRED_DATE_COL,
                className: "text-nowrap"
            },
            {
                targets: gSHIPPED_DATE_COL,
                className: "text-nowrap"
            },
            {
                targets: gTOTAL_ORDER_AMOUNT,
                render: function (data, type) {
                    return data.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }
            },
            {
                targets: gSTATUS_COL,
                render: function (data, type) {
                    switch (data) {
                        case "Pending":
                            return "<span class='status-btn close-btn'>Pending</span>";
                        case "Refund":
                            return "<span class='status-btn secondary-btn'>Refund</span>";
                        case "Completed":
                            return "<span class='status-btn success-btn'>Completed</span>";
                        case "Canceled":
                            return "<span class='status-btn dark-btn'>Canceled</span>";

                    }
                    return "";
                },
                className: "text-nowrap"
            }
        ],
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        scrollY: 400,
        scrollX: true,
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện khi nhập vào ô tìm kiếm
    $("#inp-search").on("input", function () {
        onSearchInput();
    });

    //Gán sự kiện cho nút add
    $("#add-order").on("click", function () {
        onAddOrderClick();
    });

    //Gán sự kiện cho nút Filter
    $("#filter-order").on("click", onBtnFilterClick);

    //Gán sự kiện cho nút Filter trên modal filter
    $("#btn-filter").on("click", onBtnModalFilterClick);

    //Gán sự kiện cho nút Reset trên modal filter
    $("#btn-reset-filter").on("click", onBtnModalResetFilterClick);

    //Gán sự kiện cho nút Detail
    $("#table-orders tbody").on("click", ".item-detail", function () {
        onItemDetailClick(this);
    });

    //Gán sự kiện cho nút Edit
    $("#table-orders tbody").on("click", ".item-edit", function () {
        onItemEditClick(this);
    });

    //Gán sự kiện cho nút Delete
    $("#table-orders tbody").on("click", ".item-delete", function () {
        onItemDeleteClick(this);
    });

    //Gán sự kiện cho nút Edit trên modal Detail
    $("#btn-edit").on("click", onModalDetailEditClick);

    //Gán sự kiện cho nút Delete trên modal Detail
    $("#btn-delete").on("click", onModalDetailDeleteClick);

    //Gán sự kiện cho nút Delete trên modal
    $("#btn-delete-confirm").on("click", onBtnDeleteConfirmClick);

    //Gán sự kiện cho các nút phân trang
    $("#number-page").on("keypress", function (event) {
        if (event.which == 13) {
            onNumberPageEnter();
        }
    });
    $("#sel-page-size").on("change", onChangePageSize);
    $("#first-page").on("click", onFirstPageClick);
    $("#previous-page").on("click", onPreviousPageClick);
    $("#next-page").on("click", onNextPageClick);
    $("#last-page").on("click", onLastPageClick);

    //Điều chỉnh lại các cột table khi thay đổi kích thước window
    $(window).resize(function () {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        var vCustomerId = sessionStorage.getItem("customerId");
        if (vCustomerId) {
            gCustomerId = vCustomerId;
            sessionStorage.removeItem("customerId");
        }

        loadOrderDataToTable();

        setDaterangepickerOrderDate();
        setDaterangepickerRequiredDate();

        var token = getCookie("token");
        if (token) {
            $("#sel-customer").select2({
                theme: "bootstrap-5",
                placeholder: "Enter Name / Phone Number",
                minimumInputLength: 2,
                allowClear: true,
                dropdownParent: $("#modal-filter"),
                ajax: {
                    url: gBASE_URL + "users/select-search",
                    type: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    dataType: "json",
                    delay: 250,
                    data: function (params) {
                        return {
                            keyword: params.term
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (user) {
                                return {
                                    text: user.firstName + " " + user.lastName + " - " + user.username,
                                    id: user.id,
                                    data: user
                                };
                            })
                        };
                    }
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    // Hàm xử lý khi nhập vào ô tìm kiếm
    function onSearchInput() {
        "use strict";
        gKeyword = $("#inp-search").val();
        gPage = 0;
        loadOrderDataToTable();
    }

    // Hàm xử lý khi click nút add
    function onAddOrderClick() {
        "use strict";
        window.location.href = "add-order.html";
    }

    // Hàm xử lý khi click nút Filter
    function onBtnFilterClick() {
        "use strict";
        $("#sel-sortBy").val(gSortBy);
        if (gCustomerId != "-1") {
            loadDataToSelectCustomer(gCustomerId);
        } else {
            $("#sel-customer").html("");
        }
        $("#inp-orderDate").data("daterangepicker").setStartDate(gOrderDate[0]);
        $("#inp-orderDate").data("daterangepicker").setEndDate(gOrderDate[1]);
        $("#inp-requiredDate").data("daterangepicker").setStartDate(gRequiredDate[0]);
        $("#inp-requiredDate").data("daterangepicker").setEndDate(gRequiredDate[1]);
        $("#sel-status").val(gStatus);
        $("#modal-filter").modal("show");
    }

    // Hàm xử lý khi click nút Filter trên modal filter
    function onBtnModalFilterClick() {
        "use strict";
        gSortBy = $("#sel-sortBy").val();
        gCustomerId = $("#sel-customer").val() == null ? "-1" : $("#sel-customer").val();
        gOrderDate = [$("#inp-orderDate").data("daterangepicker").startDate.format("YYYY-MM-DD"), $("#inp-orderDate").data("daterangepicker").endDate.format("YYYY-MM-DD")];
        gRequiredDate = [$("#inp-requiredDate").data("daterangepicker").startDate.format("YYYY-MM-DD"), $("#inp-requiredDate").data("daterangepicker").endDate.format("YYYY-MM-DD")];
        gStatus = $("#sel-status").val();
        gPage = 0;
        loadOrderDataToTable();
        $("#modal-filter").modal("hide");
    }

    // Hàm xử lý khi click nút Reset trên modal filter
    function onBtnModalResetFilterClick() {
        "use strict";
        gSortBy = "0";
        gCustomerId = "-1";
        gOrderDate = [gStartDate, gEndOrderDate];
        gRequiredDate = [gStartDate, gEndRequiredDate];
        gStatus = "All";
        loadOrderDataToTable();
        $("#modal-filter").modal("hide");
    }

    // Hàm xử lý khi click nút Detail
    function onItemDetailClick(paramBtnDetail) {
        "use strict";
        gId = gDataTable.row(paramBtnDetail.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "orders/" + gId,
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    loadDetailData(res);
                    $("#modal-detail").modal("show");
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    // Hàm xử lý khi click nút edit
    function onItemEditClick(paramBtnDetail) {
        "use strict";
        gId = gDataTable.row(paramBtnDetail.closest("tr")).data().id;
        window.location.href = "edit-order.html?id=" + gId;
    }

    // Hàm xử lý khi click nút Delete
    function onItemDeleteClick(paramBtnDetail) {
        "use strict";
        gId = gDataTable.row(paramBtnDetail.closest("tr")).data().id;
        $("#modal-delete").modal("show");
    }

    // Hàm xử lý khi click nút Edit trên Modal Detail
    function onModalDetailEditClick() {
        "use strict";
        $("#modal-detail").modal("hide");
        window.location.href = "edit-order.html?id=" + gId;
    }

    // Hàm xử lý khi click nút Delete trên Modal Detail
    function onModalDetailDeleteClick() {
        "use strict";
        $("#modal-detail").modal("hide");
        $("#modal-delete").modal("show");
    }

    // Hàm xử lý khi click nút delete trên modal
    function onBtnDeleteConfirmClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "orders/" + gId,
                method: "DELETE",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    $("#modal-delete").modal("hide");
                    loadOrderDataToTable();
                }, error: function (err) {
                    $("#modal-delete").modal("hide");
                    $("#modal-error").modal("show");
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    function onNumberPageEnter() {
        var vPage = $("#number-page").val();
        if (vPage < 1) {
            gPage = 0;
            loadOrderDataToTable();
        } else if (vPage > gTotalPages && gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadOrderDataToTable();
        } else {
            gPage = vPage - 1;
            loadOrderDataToTable();
        }
    }

    function onChangePageSize() {
        gPage = 0;
        gPageSize = $("#sel-page-size").val();
        loadOrderDataToTable();
    }

    //Hàm xử lý khi ấn nút Trang đầu
    function onFirstPageClick() {
        if (gTotalPages > 0) {
            gPage = 0;
            loadOrderDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang trước
    function onPreviousPageClick() {
        if (gTotalPages > 0 && gPage > 0) {
            gPage--;
            loadOrderDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang sau
    function onNextPageClick() {
        if (gTotalPages > 0 && gPage < gTotalPages - 1) {
            gPage++;
            loadOrderDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang cuối
    function onLastPageClick() {
        if (gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadOrderDataToTable();
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm load dữ liệu Order vào bảng
    function loadOrderDataToTable() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "orders?keyword=" + gKeyword + "&customerId=" + gCustomerId + "&startOrderDateString=" + gOrderDate[0] + "&endOrderDateString=" + gOrderDate[1] + "&startRequiredDateString=" + gRequiredDate[0] + "&endRequiredDateString=" + gRequiredDate[1] + "&status=" + gStatus + "&sortBy=" + gSortBy + "&page=" + gPage + "&size=" + gPageSize,
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    gDataTable.clear();
                    gDataTable.rows.add(res.content);
                    gDataTable.draw();
                    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
                    gTotalPages = res.totalPages;
                    $("#number-page").val(gPage + 1);
                    $("#element-first").html(gPage * gPageSize + (res.numberOfElements !== 0 ? 1 : 0));
                    $("#element-last").html(gPage * gPageSize + res.numberOfElements);
                    $("#total-elements").html(res.totalElements);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm load dữ liệu vào select customer
    function loadDataToSelectCustomer(paranId) {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "users/" + paranId,
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    var vText = res.firstName + " " + res.lastName + " - " + res.username;
                    var newOption = new Option(vText, res.id, true, true);
                    $("#sel-customer").append(newOption).trigger("change");
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý hiển thị chi tiết
    function loadDetailData(paramData) {
        "use strict";
        $("#p-id").html(paramData.id);
        $("#p-orderCode").html(paramData.orderCode);
        $("#p-customer").html(paramData.user.firstName + " " + paramData.user.lastName + " - " + paramData.user.username);
        $("#p-orderDate").html(paramData.orderDate);
        $("#p-requiredDate").html(paramData.requiredDate);
        $("#p-shippedDate").html(paramData.shippedDate);
        $("#p-status").html(paramData.status);
        $("#p-comments").html(paramData.comments);
        $("#p-createdAt").html(paramData.createdAt);
        $("#p-updatedAt").html(paramData.updatedAt);
        $("#p-createdBy").html(paramData.createdBy);
        $("#p-updatedBy").html(paramData.updatedBy);

        $("#p-contactName").html(paramData.firstName + " " + paramData.lastName);
        $("#p-phoneNumber").html(paramData.phoneNumber);
        $("#p-address").html(paramData.address);
        $("#p-city").html(paramData.city);
        $("#p-state").html(paramData.state);
        $("#p-postalCode").html(paramData.postalCode);
        $("#p-country").html(paramData.country);

        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "orders/" + gId + "/order-details",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    loadOrderDetailDataToTable(res);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm load dữ liệu Order Detail
    function loadOrderDetailDataToTable(paramData) {
        "use strict";
        var vText = "";
        paramData.forEach(element => {
            vText +=
                `<tr>
                    <td>${element.product.fullName} (${element.product.productCode})</td>
                    <td>${element.quantityOrder}</td>
                    <td>$${element.priceEach.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                    <td>$${(element.priceEach * element.quantityOrder).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                </tr>`
        });
        $("#tbody-order-details").html(vText);
    }

    // Hàm set daterangepicker
    function setDaterangepickerOrderDate() {
        "use strict";
        var start = new Date(gStartDate);
        var end = gEndOrderDate;
        $("#inp-orderDate").daterangepicker({
            startDate: start,
            endDate: end,
            minYear: 2021,
            maxYear: parseInt(moment().format("YYYY")),
            locale: {
                format: "YYYY/MM/DD",
                "firstDay": 1
            },
            ranges: {
                "All": [gStartDate, moment()],
                "Today": [moment(), moment()],
                "Yesterday": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                "Last 7 Days": [moment().subtract(6, "days"), moment()],
                "Last 30 Days": [moment().subtract(29, "days"), moment()],
                "This Month": [moment().startOf("month"), moment().endOf("month")],
                "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
            }
        });
    };

    function setDaterangepickerRequiredDate() {
        "use strict";
        var start = new Date(gStartDate);
        var end = new Date(gEndOrderDate);
        $("#inp-requiredDate").daterangepicker({
            startDate: start,
            endDate: end,
            minYear: 2021,
            maxYear: 2050,
            locale: {
                format: "YYYY/MM/DD",
                "firstDay": 1
            },
            ranges: {
                "All": [gStartDate, gEndRequiredDate],
                "Today": [moment(), moment()],
                "Tomorrow": [moment().add(1, "days"), moment().add(1, "days")],
                "Next 7 Days": [moment(), moment().add(6, "days")],
                "Next 30 Days": [moment(), moment().add(29, "days")],
                "This Month": [moment().startOf("month"), moment().endOf("month")],
                "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
            }
        });
    };
});