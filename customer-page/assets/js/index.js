"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gStartDateTrendingProduct = moment().subtract(30, 'days').format("YYYY-MM-DD");
var gEndDateTrendingProduct = moment().format("YYYY-MM-DD");

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
onPageLoading();

//Gán sự kiện cho các nút Shop Now
$("#shop-new-arrivals").on("click", function () {
    sessionStorage.setItem("sortBy", "4");
    window.location.href = "products.html";
});
$("#shop-big-sale").on("click", function () {
    sessionStorage.setItem("sortBy", "1");
    window.location.href = "products.html";
});
$("#shop-product-line").on("click", function () {
    sessionStorage.setItem("productBrandId", "1");
    sessionStorage.setItem("sortBy", "4");
    window.location.href = "products.html";
});
$("#shop-discount").on("click", function () {
    sessionStorage.setItem("sortBy", "1");
    window.location.href = "products.html";
});

//Gán sự kiện cho nút Add to Cart
$("#trending-product").on("click", ".add-to-cart", function () {
    onBtnAddToCart($(this));
});
$("#special-offer").on("click", ".add-to-cart", function () {
    onBtnAddToCart($(this));
});

//Gán sự kiện click Product Brand
$("#brands-logo").on("click", ".brand-logo", function () {
    var vProductBrandId = $(this)[0].dataset.productBrandId;
    sessionStorage.setItem("productBrandId", vProductBrandId);
    window.location.href = "products.html";
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    "use strict";
    $.ajax({
        url: gBASE_URL + "products/search?sortBy=4&size=1",
        method: "GET",
        async: false,
        success: function (res) {
            var vProduct = res.content[0];
            var vProductPhotos = vProduct.productPhotos;
            var vImageUrl = vProductPhotos != null && vProductPhotos.length > 0 ? gBASE_URL + "product-photos/" + vProductPhotos[0].name : "./assets/images/no-image.png";
            $("#single-slider-1").css("background-image", "url(" + vImageUrl + ")");
            $("#h-name-1").append(vProduct.productBrand.name + (vProduct.productLine != null ? " " + vProduct.productLine.name + " " : " ") + vProduct.productName);
            $("#h-price-1").append(vProduct.priceEach.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        },
        error: function (err) {
            console.log(err.responseText);
        }
    });

    $.ajax({
        url: gBASE_URL + "products/search?sortBy=1&size=1",
        method: "GET",
        async: false,
        success: function (res) {
            var vProduct = res.content[0];
            var vProductPhotos = vProduct.productPhotos;
            var vImageUrl = vProductPhotos != null && vProductPhotos.length > 0 ? gBASE_URL + "product-photos/" + vProductPhotos[0].name : "./assets/images/no-image.png";
            $("#single-slider-2").css("background-image", "url(" + vImageUrl + ")");
            $("#h-name-2").append(vProduct.productBrand.name + (vProduct.productLine != null ? " " + vProduct.productLine.name + " " : " ") + vProduct.productName);
            $("#h-price-2").append(vProduct.priceEach.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        },
        error: function (err) {
            console.log(err.responseText);
        }
    });

    $.ajax({
        url: gBASE_URL + "products/best-sellers?startDateString=" + gStartDateTrendingProduct + "&endDateString=" + gEndDateTrendingProduct + "&size=8",
        method: "GET",
        async: false,
        success: function (res) {
            loadDataProductToList(res.content, $("#trending-product"));
        },
        error: function (err) {
            console.log(err.responseText);
        }
    });

    $.ajax({
        url: gBASE_URL + "products/search?sortBy=0&size=8",
        method: "GET",
        async: false,
        success: function (res) {
            loadDataProductToList(res.content, $("#special-offer"));
        },
        error: function (err) {
            console.log(err.responseText);
        }
    });

    $.ajax({
        url: gBASE_URL + "product-brands/all",
        method: "GET",
        async: false,
        success: function (res) {
            loadDataToProductBrand(res);
        },
        error: function (err) {
            console.log(err.responseText);
        }
    });
}

//Hàm xử lý khi ấn nút Add to Cart
function onBtnAddToCart(paramElement) {
    "use strict";
    var vProductId = paramElement[0].dataset.productId;
    var vOrderDetails = getCookie("order_details") ? JSON.parse(getCookie("order_details")) : "";
    if (vOrderDetails == null || vOrderDetails == "") {
        vOrderDetails = [];
    }
    var vIsElementExists = false;
    vOrderDetails.forEach((element, index) => {
        if (element.productId == vProductId) {
            vIsElementExists = true;
            if (element.quantityOrder < 5) {
                vOrderDetails[index] = {
                    productId: vProductId,
                    quantityOrder: parseInt(element.quantityOrder) + 1
                }
            }
            toastr.success("Added to cart");
        }
    });
    if (!vIsElementExists) {
        var vOrderDetail = {
            productId: vProductId,
            quantityOrder: 1
        }
        vOrderDetails.push(vOrderDetail);
        toastr.success("Added to cart");
    }
    var vOrderDetailsJson = JSON.stringify(vOrderDetails);
    setCookie("order_details", vOrderDetailsJson, 1, true);

    var vTotalItems = 0;
    vOrderDetails.forEach(element => {
        vTotalItems += parseInt(element.quantityOrder);
    });
    $("#total-cart-items-1").html(vTotalItems);
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm hiển thị dữ liệu Product
function loadDataProductToList(paramData, paramElement) {
    "use strict";
    paramData.forEach(element => {
        var vProductId = element.id;
        var vProductName = element.fullName;
        var vProcessor = element.processor.fullName;
        var vGraphics = element.graphics != null ? element.graphics.fullName + " " + element.graphics.gpuMemory + "GB" : element.processor.processorGraphics != null ? element.processor.processorGraphics : "";
        var vScreen = element.screenSize + " inch";
        var vMemory = element.systemMemory + " GB";
        var vStorage = element.storageType + " " + element.totalStorageCapacity + " GB"
        var vWeight = element.productWeight + " kg";
        var vProductPhotos = element.productPhotos;
        var vImageSrc = "";
        if (vProductPhotos != null && vProductPhotos.length > 0) {
            vImageSrc = gBASE_URL + "product-photos/" + vProductPhotos[0].name;
        } else {
            vImageSrc = "./assets/images/no-image.png";
        }
        var vPriceEach = "$" + element.priceEach.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var vBuyPrice = element.buyPrice > element.priceEach ? "<span class=discount-price>" + "$" + element.buyPrice.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "</span>" : "";
        var vDiscountPercentage = element.discountPercentage != null && element.discountPercentage != 0 ? "<span class='sale-tag'>" + "-" + element.discountPercentage + "%" + "</span>" : "";

        var vText =
            `<div class="col-lg-3 col-md-6 col-12 mt-4">
                <div class="single-product">
                    <div class="product-image">
                        <img src="${vImageSrc}" alt="#">
                        ${vDiscountPercentage}
                        <div class="button">
                            <a href="javascript:void(0)" class="btn add-to-cart" data-product-id="${vProductId}"><i
                                    class="fa-solid fa-cart-plus"></i> Add to Cart</a>
                        </div>
                    </div>
                    <div class="product-info">
                        <h4 class="title">
                            <a href="product-details.html?id=${vProductId}">${vProductName}</a>
                        </h4>
                        <ul class="specifications">
                            <li><i class="fa-solid fa-display" title="Screen"></i><span>${vScreen}</span></li>
                            <li><i class="fa-solid fa-microchip" title="CPU"></i><span>${vProcessor}</span></li>
                            <li><i class="fa-solid fa-memory" title="RAM"></i><span>${vMemory}</span></li>
                            <li><i class="fa-solid fa-hard-drive" title="Hard Drive"></i><span>${vStorage}</span></li>
                            <li><i class="fa-solid fa-microchip" title="Graphics"></i><span>${vGraphics}</span></li>
                            <li><i class="fa-solid fa-weight-hanging" title="Weight"></i><span>${vWeight}</span></li>
                        </ul>
                        <div class="price">
                            <span>${vPriceEach}</span>
                            ${vBuyPrice}
                        </div>
                    </div>
                </div>
            </div>`;
        paramElement.append(vText);
    });
}

//Hàm load dữ liệu vào Product Brand
function loadDataToProductBrand(paramData) {
    "use strict";
    var vText = "";
    for (var bI = 0; bI < paramData.length; bI++) {
        var bId = paramData[bI].id;
        var bName = paramData[bI].name;
        var bProductBrandPhoto = paramData[bI].productBrandPhoto;
        var vImageSrc = "";
        if (bProductBrandPhoto != null) {
            vImageSrc = gBASE_URL + "product-brand-photos/" + bProductBrandPhoto.name;
        } else {
            vImageSrc = "./assets/images/no-image.png";
        }
        vText +=
            `<div class="brand-logo" data-product-brand-id="${bId}" title="${bName}">
                <a href="products.html"><img src="${vImageSrc}" alt="#"></a>
            </div>`;
    }
    $("#brands-logo").html(vText + vText);
}
