$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Kiểm tra dữ liệu nhập vào trên form đổi mật khẩu
    $("#form-change-password").validate({
        submitHandler: function () {
            changePasswordFunction();
        },
        rules: {
            inp_current_password: {
                required: true,
                minlength: 6
            },
            inp_new_password: {
                required: true,
                minlength: 6
            },
            inp_confirm_new_password: {
                required: true,
                minlength: 6,
                equalTo: "#inp-new-password"
            }
        },
        messages: {
            inp_current_password: {
                required: "Please provide a Password.",
                minlength: "Password must has at least 6 characters"
            },
            inp_new_password: {
                required: "Please provide a Password.",
                minlength: "Password must has at least 6 characters"
            },
            inp_confirm_new_password: {
                required: "Please provide a Password.",
                minlength: "Password must has at least 6 characters",
                equalTo: "Passwords do not match."
            }
        },
        errorElement: 'span',
        errorClass: 'invalid-feedback',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        checkCookie();
    }

    //Hàm xử lý Đổi mật khẩu
    function changePasswordFunction() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vCurrentPassword = $("#inp-current-password").val().trim();
            var vNewPassword = $("#inp-new-password").val().trim();
            $.ajax({
                url: gBASE_URL + "auth/change-password?currentPassword=" + vCurrentPassword + "&newPassword=" + vNewPassword,
                method: "PUT",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    if (res) {
                        setCookie("token", "", 10, false);
                        sessionStorage.setItem("changePasswordSuccess", 1);
                        window.location.href = "login.html";
                    } else {
                        toastr.error("The current password is incorrect!");
                    }
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.warning("You are not logged in!");
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm kiểm tra cookie
    function checkCookie() {
        "use strict";
        const token = getCookie("token");
        if (!token) {
            window.location.href = "login.html";
        }
    }
});