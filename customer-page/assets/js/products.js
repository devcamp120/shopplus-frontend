"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gPageSize = 12;
var gPage = 0;
var gKeyword = "";
var gSortBy = "0";
var gProductBrandId = "-1";
var gProductLineId = "-1";
var gScreenSize = [12, 20];
var gPrice = [100, 100000];
var gMemory = "-1";
var gStorageSize = "-1";

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
onPageLoading();

//Gán sự kiện cho select filter
$("#sel-productBrand").on("change", function () {
    gPage = 0;
    gProductBrandId = $("#sel-productBrand").val()
    $("#gird-product").html("");
    $("#list-product").html("");
    onSelectProductBrandChange();
    gProductLineId = "-1";
    getProductData();
});

$("#sel-productLine").on("change", function () {
    gPage = 0;
    gProductLineId = $("#sel-productLine").val()
    $("#gird-product").html("");
    $("#list-product").html("");
    getProductData();
});

$("#sel-screenSize").on("change", function () {
    gPage = 0;
    gScreenSize = JSON.parse($("#sel-screenSize").val());
    $("#gird-product").html("");
    $("#list-product").html("");
    getProductData();
});

$("#sel-price").on("change", function () {
    gPage = 0;
    gPrice = JSON.parse($("#sel-price").val());
    $("#gird-product").html("");
    $("#list-product").html("");
    getProductData();
});

$("#sel-memory").on("change", function () {
    gPage = 0;
    gMemory = $("#sel-memory").val();
    $("#gird-product").html("");
    $("#list-product").html("");
    getProductData();
});

$("#sel-storageSize").on("change", function () {
    gPage = 0;
    gStorageSize = $("#sel-storageSize").val();
    $("#gird-product").html("");
    $("#list-product").html("");
    getProductData();
});

$("#sel-sorting").on("change", function () {
    gPage = 0;
    gSortBy = $("#sel-sorting").val();
    $("#gird-product").html("");
    $("#list-product").html("");
    getProductData();
});

//Gán sự kiện cho nút View More
$("#view-more").on("click", onBtnViewMoreClick);

//Gán sự kiện cho nút Add to Cart
$("#gird-product").on("click", ".add-to-cart", function () {
    onBtnAddToCart($(this));
});
$("#list-product").on("click", ".add-to-cart", function () {
    onBtnAddToCart($(this));
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    "use strict";
    $.ajax({
        url: gBASE_URL + "product-brands/all",
        method: "GET",
        async: false,
        success: function (res) {
            loadDataToSelectProductBrand(res);
        },
        error: function (err) {
            console.log(err.responseText);
        }
    });

    var vKeyword = sessionStorage.getItem("searchKeyword");
    if (vKeyword) {
        gKeyword = vKeyword;
        $("#page-title").append(" / Search for: '" + gKeyword + "'");
        sessionStorage.removeItem("searchKeyword");
    }

    var vSortBy = sessionStorage.getItem("sortBy");
    if (vSortBy) {
        gSortBy = vSortBy;
        $("#sel-sorting").val(gSortBy);
        sessionStorage.removeItem("sortBy");
    }

    var vProductBrandId = sessionStorage.getItem("productBrandId");
    if (vProductBrandId) {
        gProductBrandId = vProductBrandId;
        $("#sel-productBrand").val(gProductBrandId);
        onSelectProductBrandChange();
        sessionStorage.removeItem("productBrandId");

        var vProductLineId = sessionStorage.getItem("productLineId");
        if (vProductLineId) {
            gProductLineId = vProductLineId;
            $("#sel-productLine").val(gProductLineId);
            sessionStorage.removeItem("productLineId");
        }
    }

    $("#gird-product").html("");
    $("#list-product").html("");
    getProductData();
}

//Hàm xử lý khi chọn Product Brand
function onSelectProductBrandChange() {
    var vProductBrandId = $("#sel-productBrand").val();
    $("#sel-productLine").html("");
    $("#sel-productLine").append($("<option>").val("-1").text("All"));
    if (vProductBrandId != "-1") {
        $.ajax({
            url: "http://localhost:8080/product-brands/" + vProductBrandId + "/product-lines",
            method: "GET",
            async: false,
            success: function (res) {
                $("#sel-productLine").prop("disabled", false);
                loadDataToSelectProductLine(res);
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
    } else {
        $("#sel-productLine").val("-1");
        $("#sel-productLine").prop("disabled", true);
    }
}

//Hàm xử lý khi ấn nút View More
function onBtnViewMoreClick() {
    "use strict";
    gPage++;
    getProductData();
}

//Hàm xử lý khi ấn nút Add to Cart
function onBtnAddToCart(paramElement) {
    "use strict";
    var vProductId = paramElement[0].dataset.productId;
    var vOrderDetails = getCookie("order_details") ? JSON.parse(getCookie("order_details")) : "";
    if (vOrderDetails == null || vOrderDetails == "") {
        vOrderDetails = [];
    }
    var vIsElementExists = false;
    vOrderDetails.forEach((element, index) => {
        if (element.productId == vProductId) {
            vIsElementExists = true;
            if (element.quantityOrder < 5) {
                vOrderDetails[index] = {
                    productId: vProductId,
                    quantityOrder: parseInt(element.quantityOrder) + 1
                }
            }
            toastr.success("Added to cart");
        }
    });
    if (!vIsElementExists) {
        var vProduct = {
            productId: vProductId,
            quantityOrder: 1
        }
        vOrderDetails.push(vProduct);
        toastr.success("Added to cart");
    }
    var vOrderDetailsJson = JSON.stringify(vOrderDetails);
    setCookie("order_details", vOrderDetailsJson, 1, true);

    var vTotalItems = 0;
    vOrderDetails.forEach(element => {
        vTotalItems += parseInt(element.quantityOrder);
    });
    $("#total-cart-items-1").html(vTotalItems);
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm load dữ liệu vào Select Product Brand
function loadDataToSelectProductBrand(paramData) {
    "use strict";
    for (var bI = 0; bI < paramData.length; bI++) {
        var bId = paramData[bI].id;
        var bName = paramData[bI].name;
        $("#sel-productBrand").append($("<option>").val(bId).text(bName));
    }
}

//Hàm load dữ liệu vào Select Product Line
function loadDataToSelectProductLine(paramData) {
    "use strict";
    for (var bI = 0; bI < paramData.length; bI++) {
        var bId = paramData[bI].id;
        var bName = paramData[bI].name;
        $("#sel-productLine").append($("<option>").val(bId).text(bName));
    }
}

//Hàm lấy dữ liệu product
function getProductData() {
    "use strict";
    $.ajax({
        url: gBASE_URL + "products/search?keyword=" + gKeyword + "&productBrandId=" + gProductBrandId + "&productLineId=" + gProductLineId + "&minScreenSize=" + gScreenSize[0] + "&maxScreenSize=" + gScreenSize[1] + "&minPrice=" + gPrice[0] + "&maxPrice=" + gPrice[1] + "&memory=" + gMemory + "&storageSize=" + gStorageSize + "&sortBy=" + gSortBy + "&page=" + gPage,
        method: "GET",
        success: function (res) {
            loadProductData(res.content);
            $("#total-show-product").html((res.numberOfElements !== 0 ? 1 : 0) + " - " + (gPage * gPageSize + res.numberOfElements) + " items");
            if (gPage >= res.totalPages - 1) {
                if (res.totalPages == 0)
                    gPage = 0;
                else
                    gPage = res.totalPages - 1;
                $("#view-more").prop("hidden", true);
            } else {
                $("#view-more").prop("hidden", false);
            }
        },
        error: function (err) {
            console.log(err.responseText);
        }
    });
}

//Hàm hiển thị dữ liệu product
function loadProductData(paramData) {
    "use strict";
    paramData.forEach(element => {
        var vProductId = element.id;
        var vProductName = element.fullName;
        var vProcessor = element.processor.fullName;
        var vGraphics = element.graphics != null ? element.graphics.fullName + " " + element.graphics.gpuMemory + "GB" : element.processor.processorGraphics != null ? element.processor.processorGraphics : "";
        var vScreen = element.screenSize + " inch";
        var vMemory = element.systemMemory + " GB";
        var vStorage = element.storageType + " " + element.totalStorageCapacity + " GB"
        var vWeight = element.productWeight + " kg";
        var vProductPhotos = element.productPhotos;
        var vImageSrc = "";
        if (vProductPhotos != null && vProductPhotos.length > 0) {
            vImageSrc = gBASE_URL + "product-photos/" + vProductPhotos[0].name;
        } else {
            vImageSrc = "./assets/images/no-image.png";
        }
        var vPriceEach = "$" + element.priceEach.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var vBuyPrice = element.buyPrice > element.priceEach ? "<span class=discount-price>" + "$" + element.buyPrice.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "</span>" : "";
        var vDiscountPercentage = element.discountPercentage != null && element.discountPercentage != 0 ? "<span class='sale-tag'>" + "-" + element.discountPercentage + "%" + "</span>" : "";

        var vTextGird =
            `<div class="col-lg-4 col-md-6 col-12 mt-4">
                <div class="single-product">
                    <div class="product-image">
                        <img src="${vImageSrc}" alt="#">
                        ${vDiscountPercentage}
                        <div class="button">
                            <a href="javascript:void(0)" class="btn add-to-cart" data-product-id="${vProductId}"><i
                                    class="fa-solid fa-cart-plus"></i> Add to Cart</a>
                        </div>
                    </div>
                    <div class="product-info">
                        <h4 class="title">
                            <a href="product-details.html?id=${vProductId}">${vProductName}</a>
                        </h4>
                        <ul class="specifications">
                            <li><i class="fa-solid fa-display" title="Screen"></i><span>${vScreen}</span></li>
                            <li><i class="fa-solid fa-microchip" title="CPU"></i><span>${vProcessor}</span></li>
                            <li><i class="fa-solid fa-memory" title="RAM"></i><span>${vMemory}</span></li>
                            <li><i class="fa-solid fa-hard-drive" title="Hard Drive"></i><span>${vStorage}</span></li>
                            <li><i class="fa-solid fa-microchip" title="Graphics"></i><span>${vGraphics}</span></li>
                            <li><i class="fa-solid fa-weight-hanging" title="Weight"></i><span>${vWeight}</span></li>
                        </ul>
                        <div class="price">
                            <span>${vPriceEach}</span>
                            ${vBuyPrice}
                        </div>
                    </div>
                </div>
            </div>`;

        var vTextList =
            `<div class="col-lg-12 col-md-12 col-12 mt-4">
                <div class="single-product">
                    <div class="row align-items-center">
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="product-image">
                                <img src="${vImageSrc}" alt="#">
                                ${vDiscountPercentage}
                                <div class="button">
                                    <a href="javascript:void(0)" class="btn add-to-cart" data-product-id="${vProductId}"><i
                                            class="fa-solid fa-cart-plus"></i> Add to Cart</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-12">
                            <div class="product-info">
                                <h4 class="title">
                                    <a href="product-details.html?id=${vProductId}">${vProductName}</a>
                                </h4>
                                <ul class="specifications">
                                    <li><i class="fa-solid fa-display" title="Screen"></i><span>${vScreen}</span></li>
                                    <li><i class="fa-solid fa-microchip" title="CPU"></i><span>${vProcessor}</span></li>
                                    <li><i class="fa-solid fa-memory" title="RAM"></i><span>${vMemory}</span></li>
                                    <li><i class="fa-solid fa-hard-drive" title="Hard Drive"></i><span>${vStorage}</span>
                                    </li>
                                    <li><i class="fa-solid fa-microchip" title="Graphics"></i><span>${vGraphics}</span></li>
            
                                    <li><i class="fa-solid fa-weight-hanging" title="Weight"></i><span>${vWeight}</span>
                                    </li>
                                </ul>
                                <div class="price">
                                    <span>${vPriceEach}</span>
                                    ${vBuyPrice}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`;

        $("#gird-product").append(vTextGird);
        $("#list-product").append(vTextList);
    });
}
